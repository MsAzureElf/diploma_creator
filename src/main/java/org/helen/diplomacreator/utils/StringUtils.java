package org.helen.diplomacreator.utils;

import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by Helen on 27.03.2016.
 */
public class StringUtils {

    public static ArrayList<String> split(String value, String delimiter){
        StringTokenizer st = new StringTokenizer(value, delimiter, false);
        ArrayList<String> ret = new ArrayList<>();
        while (st.hasMoreTokens())
            ret.add(st.nextToken());
        return ret;
    }

    public static String removeExtraSpaces(String value){
        String newValue = value.replace("  ", " ");
        while (!newValue.contentEquals(value)){
            value = newValue;
            newValue = value.replace("  ", " ");
        }
        return newValue;
    }

    public static String replaceForbidden(String value, String [] forbiddenCharacters){
        for (String forbidden : forbiddenCharacters)
            value = value.replace(forbidden, "");
        return value;
    }

}
