package org.helen.diplomacreator.utils;

import java.io.*;
import java.time.format.DateTimeFormatter;

/**
 * Created by Helen on 03.04.2016.
 */
public class Logger {

    private static final String FILE_NAME = "log.log";

    private static final Logger instance = new Logger();

    private PrintWriter writer = null;

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy hh:mm:ss");

    PrintStream printStream;

    public static Logger getInstance() {
        return instance;
    }

    private Logger() {
        File file = new File(FILE_NAME);
        if (!file.exists() || file.isDirectory()) {
            try {
                printStream = new PrintStream(file);
                writer = new PrintWriter(printStream);
            } catch (FileNotFoundException e) {
                printStream = new PrintStream(System.out);
            }
        }
    }

    public void log(String message) {
        if (writer == null)
            return;
        try {
            writer.println(/*LocalDate.now().format(formatter) + ": " +*/ message);
            writer.flush();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public PrintStream getStream(){
        return printStream;
    }

}
