package org.helen.diplomacreator;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.helen.diplomacreator.logic.MainController;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/view/window.fxml"));
        Parent root = loader.load();
        Stage companyStage = new Stage();
        companyStage.initModality(Modality.APPLICATION_MODAL);
        primaryStage.setTitle("Diploma creator");
        MainController controller = loader.getController();
        controller.setStage(primaryStage);
        Scene scene = new Scene(root);
        String css = this.getClass().getResource("/view/main.css").toExternalForm();
        scene.getStylesheets().add(css);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
