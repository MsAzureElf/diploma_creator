package org.helen.diplomacreator.model;

import java.sql.ResultSet;

/**
 * Created by Helen on 06.02.2016.
 */
public class Diploma implements DBEntity {

    private int id;
    private String fullName = "Диплом1";
    private int x = 75;
    private int y = 10;
    private String diplomaTemplate = "";
    private String competitionType = "";
    private String competitionName = "";
    private String thanksTextMentor = "";
    private String thanksTextSponsor = "";

    public boolean isOlympiad() {
        return olympiad;
    }

    public void setOlympiad(boolean olympiad) {
        this.olympiad = olympiad;
    }

    public String getThanksTextMentor() {
        return thanksTextMentor;
    }

    public void setThanksTextMentor(String thanksTextMentor) {
        this.thanksTextMentor = thanksTextMentor;
    }

    private boolean olympiad = false;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public String getDiplomaTemplate() {
        return diplomaTemplate;
    }

    public void setDiplomaTemplate(String diplomaTemplate) {
        this.diplomaTemplate = diplomaTemplate;
    }

    public String getCompetitionType() {
        return competitionType;
    }

    public void setCompetitionType(String competitionType) {
        this.competitionType = competitionType;
    }

    public String getCompetitionName() {
        return competitionName;
    }

    public void setCompetitionName(String competitionName) {
        this.competitionName = competitionName;
    }

    public Diploma(){

    }

    public Diploma(int id, String fullName, int x, int y, String diplomaTemplate, String competitionType, String competitionName, boolean olympiad, String thanksTextMentor, String thanksTextSponsor) {
        this.id = id;
        this.fullName = fullName;
        this.x = x;
        this.y = y;
        this.diplomaTemplate = diplomaTemplate;
        this.competitionType = competitionType;
        this.competitionName = competitionName;
        this.olympiad = olympiad;
        this.thanksTextMentor = thanksTextMentor;
        this.thanksTextSponsor = thanksTextSponsor;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public String DBExportString() {
        String query = "INSERT into Diploma ('name', 'x', 'y', 'diploma_template', 'olympiad', 'thanks_text_mentor', 'thanks_text_sponsor', 'competition_type', 'competition_name') values (";
        query += "'" + getFullName() + "', ";
        query += "" + getX() + ", ";
        query += "" + getY() + ", ";
        query += "'" + getDiplomaTemplate() + "', ";
        query += "" + (isOlympiad()?1:0) + ", ";
        query += "'" + getThanksTextMentor() + "', ";
        query += "'" + getThanksTextSponsor() + "', ";
        query += "'" + getCompetitionType() + "', ";
        query += "'" + getCompetitionName() + "'); ";
        return query;
    }

    @Override
    public String DBFindString() {
        String query = "SELECT * from " + getEntityType().name() + " where ";
        query += "name = '" + getFullName() + "' and ";
        query += "x = " + getX() + " and ";
        query += "y = " + getY() + " and ";
        query += "diploma_template = '" + getDiplomaTemplate() + "' and ";
        query += "olympiad = " + (isOlympiad()?1:0) + " and ";
        query += "thanks_text_mentor = '" + getThanksTextMentor() + "' and ";
        query += "thanks_text_sponsor = '" + getThanksTextSponsor() + "' and ";
        query += "competition_type = '" + getCompetitionType() + "' and ";
        query += "competition_name = '" + getCompetitionName() + "'; ";
        return query;
    }

    @Override
    public String DBModifyString() {
        String query = "UPDATE Diploma SET ";
        query += "name = '" + getFullName() + "', ";
        query += "x = " + getX() + ", ";
        query += "y = " + getY() + ", ";
        query += "diploma_template = '" + getDiplomaTemplate() + "', ";
        query += "olympiad = " + (isOlympiad()?1:0) + ", ";
        query += "thanks_text_mentor = '" + getThanksTextMentor() + "', ";
        query += "thanks_text_sponsor = '" + getThanksTextSponsor() + "', ";
        query += "competition_type = '" + getCompetitionType() + "', ";
        query += "competition_name = '" + getCompetitionName() + "' where id = " + getId();
        return query;
    }

    @Override
    public void DBImportString(ResultSet dbString) {

    }

    @Override
    public EntityType getEntityType() {
        return EntityType.DIPLOMA;
    }

    public String getThanksTextSponsor() {
        return thanksTextSponsor;
    }

    public void setThanksTextSponsor(String thanksTextSponsor) {
        this.thanksTextSponsor = thanksTextSponsor;
    }
}
