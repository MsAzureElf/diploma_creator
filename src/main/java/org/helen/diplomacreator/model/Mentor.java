package org.helen.diplomacreator.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TableColumn;

import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created by Helen on 31.01.2016.
 */
public class Mentor implements DBEntity {

    private static String [] tvMentorColNames = {"ФИО", "Должность", "Благодарственное письмо"};

    private int id;
    private int organization = 0;
    private StringProperty name = new SimpleStringProperty("");
    private StringProperty subject = new SimpleStringProperty("");
    private StringProperty position = new SimpleStringProperty("");

    private StringProperty thanksTemplate = new SimpleStringProperty("");

    public Mentor(){

    }

    public Mentor(int id, int organization, String name, String thanksTemplate, String subject, String position){
        this.id = id;
        this.name = new SimpleStringProperty(name);
        this.subject = new SimpleStringProperty(subject);
        this.organization = organization;
        this.thanksTemplate = new SimpleStringProperty(thanksTemplate);
        this.position = new SimpleStringProperty(position);
    }

    public String getSubject() {
        return subject.get();
    }

    public StringProperty subjectProperty() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject.set(subject);
    }

    public String getThanksTemplate() {
        return thanksTemplate.get();
    }

    public StringProperty thanksTemplateProperty() {
        return thanksTemplate;
    }

    public void setThanksTemplate(String thanks_template) {
        this.thanksTemplate.set(thanks_template);
    }

    public int getId(){
        return id;
    }

    @Override
    public void setId(int id) {

    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty() {
        return name;
    }

    public int getOrganization() {
        return organization;
    }

    public void setOrganization(int organization) {
        this.organization = organization;
    }

    public String getPosition() {
        return position.get();
    }

    public StringProperty positionProperty() {
        return position;
    }

    public void setPosition(String position) {
        this.position.set(position);
    }

    public static ArrayList<TableColumn<Mentor, String>> getColumns(){
        ArrayList<TableColumn<Mentor, String>> list = new ArrayList<>();
        TableColumn<Mentor, String> colFIO = new TableColumn<>(tvMentorColNames[0]);
        colFIO.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        list.add(colFIO);
        TableColumn<Mentor, String> colPosition = new TableColumn<>(tvMentorColNames[1]);
        colPosition.setCellValueFactory(cellData -> cellData.getValue().positionProperty());
        list.add(colPosition);
        TableColumn<Mentor, String> colThanks = new TableColumn<>(tvMentorColNames[2]);
        colThanks.setCellValueFactory(cellData -> cellData.getValue().thanksTemplateProperty());
        list.add(colThanks);
        return list;
    }

    @Override
    public String DBExportString() {
        String query = "INSERT into Mentor ('name', 'organization', 'subject', 'thanks_template', 'position') values (";
        query += "'" + getName() + "', ";
        query += "'" + getOrganization() + "', ";
        query += "'" + getSubject() + "', ";
        query += "'" + getThanksTemplate() + "', ";
        query += "'" + getPosition() + "'); ";
        return query;
    }

    @Override
    public String DBFindString() {
        String query = "SELECT * from " + getEntityType().name() + " where ";
        query += "name = '" + getName() + "' and ";
        query += "position = '" + getPosition() + "' and ";
        query += "subject = '" + getSubject() + "' and ";
        query += "thanks_template = '" + getThanksTemplate() + "' and ";
        query += "organization = '" + getOrganization() + "'; ";
        return query;
    }

    @Override
    public String DBModifyString() {
        String query = "UPDATE Mentor SET ";
        query += "name = '" + getName() + "', ";
        query += "position = '" + getPosition() + "', ";
        query += "subject = '" + getSubject() + "', ";
        query += "thanks_template = '" + getThanksTemplate() + "', ";
        query += "organization = '" + getOrganization() + "' where id = " + getId();
        return query;
    }

    @Override
    public void DBImportString(ResultSet dbString) {

    }

    @Override
    public EntityType getEntityType() {
        return EntityType.MENTOR;
    }
}