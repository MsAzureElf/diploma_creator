package org.helen.diplomacreator.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TableColumn;

import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created by Helen on 30.01.2016.
 */
public class Participant implements DBEntity {

    private static String[] tvParticipantsColNames = {"Номер", "Участник", "Класс", "Предмет", "Название работы", "Руководитель", "Место", "Баллы", "Диплом"};

    private IntegerProperty id = new SimpleIntegerProperty(0);
    private StringProperty name = new SimpleStringProperty("");
    private StringProperty grade = new SimpleStringProperty("");
    private StringProperty subject = new SimpleStringProperty("");
    private StringProperty workName = new SimpleStringProperty("");
    private StringProperty mentor = new SimpleStringProperty("");
    private StringProperty place = new SimpleStringProperty("");
    private StringProperty score = new SimpleStringProperty("");
    private StringProperty diploma = new SimpleStringProperty("");
    private int organization = 0;
    private boolean personalAgreement;

    public String getWorkName() {
        return workName.get();
    }

    public StringProperty workNameProperty() {
        return workName;
    }

    public void setWorkName(String workName) {
        this.workName.set(workName);
    }

    public boolean isPersonalAgreement() {
        return personalAgreement;
    }

    public void setPersonalAgreement(boolean personalAgreement) {
        this.personalAgreement = personalAgreement;
    }

    public String getDiploma() {
        return diploma.get();
    }

    public StringProperty diplomaProperty() {
        return diploma;
    }

    public void setDiploma(String diploma) {
        this.diploma.set(diploma);
    }

    public String getMentor() {
        return mentor.get();
    }

    public StringProperty mentorProperty() {
        return mentor;
    }

    public void setMentor(String mentor) {
        this.mentor.set(mentor);
    }

    public String getPlace() {
        return place.get();
    }

    public StringProperty placeProperty() {
        return place;
    }

    public void setPlace(String place) {
        this.place.set(place);
    }

    public String getScore() {
        return score.get();
    }

    public StringProperty scoreProperty() {
        return score;
    }

    public void setScore(String score) {
        this.score.set(score);
    }

    public String getSubject() {
        return subject.get();
    }

    public StringProperty subjectProperty() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject.set(subject);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public int getId() {
        return id.get();
    }

    public IntegerProperty idProperty() {
        return id;
    }

    public void setId(int id) {
        this.id.set(id);
    }

    public int getOrganization() {
        return organization;
    }

    public void setOrganization(int organization) {
        this.organization = organization;
    }

    public Participant() {

    }

    public Participant(int id, String name, String grade, String subject, String workName, String score, String place, String mentor, String diploma, int organization) {
        this.id = new SimpleIntegerProperty(id);
        this.name = new SimpleStringProperty(name);
        this.grade = new SimpleStringProperty(grade);
        this.subject = new SimpleStringProperty(subject);
        this.workName = new SimpleStringProperty(workName);
        this.mentor = new SimpleStringProperty(mentor);
        this.place = new SimpleStringProperty(place);
        this.score = new SimpleStringProperty(score);
        this.diploma = new SimpleStringProperty(diploma);
        this.organization = organization;
    }

    public static ArrayList<TableColumn<Participant, ?>> getColumns() {
        ArrayList<TableColumn<Participant, ?>> list = new ArrayList<>();
        TableColumn<Participant, Number> colId = new TableColumn<>(tvParticipantsColNames[1]);
        colId.setCellValueFactory(cellData -> cellData.getValue().idProperty());
        list.add(colId);
        TableColumn<Participant, String> colName = new TableColumn<>(tvParticipantsColNames[0]);
        colName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        list.add(colName);
        TableColumn<Participant, String> colGrade = new TableColumn<>(tvParticipantsColNames[2]);
        colGrade.setCellValueFactory(cellData -> cellData.getValue().gradeProperty());
        list.add(colGrade);
        TableColumn<Participant, String> colSubject = new TableColumn<>(tvParticipantsColNames[3]);
        colSubject.setCellValueFactory(cellData -> cellData.getValue().subjectProperty());
        list.add(colSubject);
        TableColumn<Participant, String> colWorkName = new TableColumn<>(tvParticipantsColNames[4]);
        colWorkName.setCellValueFactory(cellData -> cellData.getValue().workNameProperty());
        list.add(colWorkName);
        TableColumn<Participant, String> colMentor = new TableColumn<>(tvParticipantsColNames[5]);
        colMentor.setCellValueFactory(cellData -> cellData.getValue().mentorProperty());
        list.add(colMentor);
        TableColumn<Participant, String> colPlace = new TableColumn<>(tvParticipantsColNames[6]);
        colPlace.setCellValueFactory(cellData -> cellData.getValue().placeProperty());
        list.add(colPlace);
        TableColumn<Participant, String> colScore = new TableColumn<>(tvParticipantsColNames[7]);
        colScore.setCellValueFactory(cellData -> cellData.getValue().scoreProperty());
        list.add(colScore);
        TableColumn<Participant, String> colDiploma = new TableColumn<>(tvParticipantsColNames[8]);
        colDiploma.setCellValueFactory(cellData -> cellData.getValue().diplomaProperty());
        list.add(colDiploma);
        return list;
    }

    @Override
    public String DBExportString() {
        String query = "INSERT into Participant ('name', 'grade', 'subject', 'work_name', 'score', 'place', 'mentor', 'diploma', 'organization', 'agreement') values (";
        query += "'" + getName() + "', ";
        query += "'" + getGrade() + "', ";
        query += "'" + getSubject() + "', ";
        query += "'" + getWorkName() + "', ";
        query += "'" + getScore() + "', ";
        query += "'" + getPlace() + "', ";
        query += "'" + getMentor() + "', ";
        query += "'" + getDiploma() + "', ";
        query += "" + getOrganization() + ", ";
        query += "'" + isPersonalAgreement() + "'); ";
        return query;
    }

    @Override
    public String DBModifyString() {
        String query = "UPDATE Participant SET ";
        query += "name = '" + getName() + "', ";
        query += "grade = '" + getGrade() + "', ";
        query += "subject = '" + getSubject() + "', ";
        query += "work_name = '" + getWorkName() + "', ";
        query += "score = '" + getScore() + "', ";
        query += "place = '" + getPlace() + "', ";
        query += "mentor = '" + getMentor() + "', ";
        query += "diploma = '" + getDiploma() + "', ";
        query += "agreement = '" + isPersonalAgreement() + "' where id = " + getId();
        return query;
    }

    @Override
    public String DBFindString() {
        String query = "SELECT * from " + getEntityType().name() + " where ";
        query += "name = '" + getName() + "' and ";
        query += "grade = '" + getGrade() + "' and ";
        query += "subject = '" + getSubject() + "' and ";
        query += "work_name = '" + getWorkName() + "' and ";
        query += "score = '" + getScore() + "' and ";
        query += "place = '" + getPlace() + "' and ";
        query += "mentor = '" + getMentor() + "' and ";
        query += "diploma = '" + getDiploma() + "' and ";
        query += "organization = " + getOrganization() + " and ";
        query += "agreement = '" + isPersonalAgreement() + "';";
        return query;
    }

    @Override
    public void DBImportString(ResultSet dbString) {

    }

    @Override
    public EntityType getEntityType() {
        return EntityType.PARTICIPANT;
    }

    public String getGrade() {
        return grade.get();
    }

    public StringProperty gradeProperty() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade.set(grade);
    }

    /*enum Nomination {
        HISTORY("Историческая"),
        LITERATURE("Литературная"),
        LOGO("Разработка эмблем, логотипов, закладок школьных библиотек"),
        ADVERT("Социальная реклама"),
        EVENT("Мероприятия"),
        MEDIA("Мультимедийные издания");

        private final String value;

        Nomination(String value) {
            this.value = value;
        }

        static public Nomination getNomination(String pDir) {
            for (Nomination type : Nomination.values()) {
                if (type.toString().equalsIgnoreCase(pDir)) {
                    return type;
                }
            }
            return HISTORY;
        }

    }*/

}
