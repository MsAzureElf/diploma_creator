package org.helen.diplomacreator.model;

import java.sql.ResultSet;

/**
 * Created by Helen on 06.02.2016.
 */
public class Subject implements DBEntity {

    private int id;
    private String shortName;
    private String fullName;

    public Subject(int id, String shortName, String fullName){
        this.id = id;
        this.shortName = shortName;
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String DBExportString() {
        String query = "INSERT into Subject ('name', 'short_name') values (";
        query += "'" + getFullName() + "', ";
        query += "'" + getShortName() + "'); ";
        return query;
    }

    @Override
    public String DBFindString() {
        String query = "SELECT * from " + getEntityType().name() + " where ";
        query += "name = '" + getFullName() + "' and ";
        query += "short_name = '" + getShortName() + "'; ";
        return query;
    }

    @Override
    public String DBModifyString() {
        String query = "UPDATE Subject SET ";
        query += "name = '" + getFullName() + "', ";
        query += "short_name = '" + getShortName() + "' where id = " + getId();
        return query;
    }

    @Override
    public void DBImportString(ResultSet dbString) {

    }

    @Override
    public EntityType getEntityType() {
        return EntityType.SUBJECT;
    }
}
