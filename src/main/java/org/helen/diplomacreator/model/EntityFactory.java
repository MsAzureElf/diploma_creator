package org.helen.diplomacreator.model;

import org.helen.diplomacreator.utils.Logger;

import java.sql.ResultSet;

/**
 * Created by Helen on 06.02.2016.
 */
public class EntityFactory {

    public static DBEntity getEntity(ResultSet resultSet, DBEntity.EntityType entityType) {
        try {
            switch (entityType) {
                case DIPLOMA:
                    return new Diploma(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getInt("x"), resultSet.getInt("y"), resultSet.getString("diploma_template"),
                            resultSet.getString("competition_type"), resultSet.getString("competition_name"), resultSet.getBoolean("olympiad"), resultSet.getString("thanks_text_mentor"), resultSet.getString("thanks_text_sponsor"));
                case MENTOR: {
                    return new Mentor(resultSet.getInt("id"), resultSet.getInt("organization"), resultSet.getString("name"), resultSet.getString("thanks_template"), resultSet.getString("subject"), resultSet.getString("position"));
                }
                case ORGANIZATION:
                    return new Organization(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("address"),
                            resultSet.getString("phone"), resultSet.getString("email"), resultSet.getString("request"));
                case PARTICIPANT:
                    return new Participant(resultSet.getInt("id"), resultSet.getString("name"), resultSet.getString("grade"), resultSet.getString("subject"), resultSet.getString("work_name"), resultSet.getString("score"), resultSet.getString("place"), resultSet.getString("mentor"), resultSet.getString("diploma"), resultSet.getInt("organization"));
                case POSITION:
                    return new Position(resultSet.getInt("id"), resultSet.getString("short_name"), resultSet.getString("name"));
                case SPONSOR:
                    return new Sponsor(resultSet.getInt("id"), resultSet.getString("first_name"), resultSet.getString("last_name"),
                            resultSet.getString("father_name"), resultSet.getString("position"), resultSet.getString("phone"), resultSet.getInt("organization"), resultSet.getString("thanks_template"));
                case SUBJECT:
                    return new Subject(resultSet.getInt("id"), resultSet.getString("short_name"), resultSet.getString("name"));
            }
        } catch (Exception e) {
            Logger.getInstance().log("Unable to parse DBEntity");
            e.printStackTrace(Logger.getInstance().getStream());
        }
        return null;
    }

}
