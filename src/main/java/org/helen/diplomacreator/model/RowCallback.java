package org.helen.diplomacreator.model;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Callback;
import org.helen.diplomacreator.logic.DBConnector;
import org.helen.diplomacreator.logic.FormController;
import org.helen.diplomacreator.logic.MainController;
import org.helen.diplomacreator.utils.Logger;

/**
 * Created by Helen on 14.02.2016.
 */
public class RowCallback<T extends DBEntity> implements Callback<TableView<T>, TableRow<T>> {

    public RowCallback() {
        super();
    }

    @Override
    public TableRow<T> call(TableView<T> param) {
        TableRow row = new TableRow<T>() {
            @Override
            public void updateItem(T row, boolean b) {
                super.updateItem(row, b);
            }
        };
        row.setOnMouseClicked(event -> {
            if (event.getClickCount() == 2 && (!row.isEmpty())) {
                T rowData = param.getSelectionModel().getSelectedItem();
                if (!(rowData instanceof DBEntity))
                    return;
                String fxmlLocation = getFxmlLocation(rowData.getEntityType());
                if (fxmlLocation == null)
                    return;
                openForm(rowData, fxmlLocation);
            }
        });
        return row;
    }

    public static String getFxmlLocation(org.helen.diplomacreator.model.DBEntity.EntityType entityType) {
        switch (entityType) {
            case MENTOR:
                return "/main/org.helen.diplomacreator.view/Mentor.fxml";
            case ORGANIZATION:
                return "/main/org.helen.diplomacreator.view/Organization.fxml";
            case PARTICIPANT:
                return "/main/org.helen.diplomacreator.view/Participant.fxml";
            case SPONSOR:
                return "/main/org.helen.diplomacreator.view/Sponsor.fxml";
            case DIPLOMA:
                return "/main/org.helen.diplomacreator.view/Diploma.fxml";
        }
        return null;
    }

    public static void openForm(DBEntity data, String fxmlLocation) {
        FXMLLoader loader = new FXMLLoader(RowCallback.class.getResource(fxmlLocation));
        Stage dialogStage = new Stage();
        Pane page = null;
        try {
            page = loader.load();
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
        Scene scene = new Scene(page);
        String css = RowCallback.class.getResource("/main/org.helen.diplomacreator.view/main.css").toExternalForm();
        FormController<DBEntity> controller = loader.getController();
        controller.setData(data);
        controller.setStage(dialogStage);
        scene.getStylesheets().add(css);
        dialogStage.setScene(scene);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.showAndWait();
        FormController.DialogResult result = controller.getDialogResult();
        switch (result) {
            case OK: {
                DBEntity newData = controller.getData();
                DBConnector.getInstance().update(newData);
                MainController.getInstance().reloadTable(newData.getEntityType(), MainController.getInstance().getSelectedLVIndex(data.getEntityType()));
                break;
            }
            case DELETE: {
                DBConnector.getInstance().delete(data);
                MainController.getInstance().reloadTable(data.getEntityType(), 0);
                break;
            }
            case CANCEL:
                return;
        }
    }

}
