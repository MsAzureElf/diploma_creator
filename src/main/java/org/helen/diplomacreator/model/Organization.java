package org.helen.diplomacreator.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TableColumn;

import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created by Helen on 30.01.2016.
 */
public class Organization implements DBEntity {

    private int id;
    protected StringProperty name = new SimpleStringProperty();

    private StringProperty address = new SimpleStringProperty();
    private StringProperty phone = new SimpleStringProperty();
    private StringProperty email = new SimpleStringProperty();

    private StringProperty request = new SimpleStringProperty();

    private static String [] tvOrganizationsColNames = {"Наименование ОУ", "Адрес", "Телефон", "E-mail", "Заявка"};

    public Organization(){

    }

    public static ArrayList<TableColumn<Organization, String>> getColumns(){
        ArrayList<TableColumn<Organization, String>> list = new ArrayList<>();
        TableColumn<Organization, String> colFullName = new TableColumn<>(tvOrganizationsColNames[0]);
        colFullName.setCellValueFactory(cellData -> cellData.getValue().nameProperty());
        list.add(colFullName);
        TableColumn<Organization, String> colAddress = new TableColumn<>(tvOrganizationsColNames[1]);
        colAddress.setCellValueFactory(cellData -> cellData.getValue().addressProperty());
        list.add(colAddress);
        TableColumn<Organization, String> colPhone = new TableColumn<>(tvOrganizationsColNames[2]);
        colPhone.setCellValueFactory(cellData -> cellData.getValue().phoneProperty());
        list.add(colPhone);
        TableColumn<Organization, String> colEmail = new TableColumn<>(tvOrganizationsColNames[3]);
        colEmail.setCellValueFactory(cellData -> cellData.getValue().emailProperty());
        list.add(colEmail);
        TableColumn<Organization, String> colRequest = new TableColumn<>(tvOrganizationsColNames[4]);
        colRequest.setCellValueFactory(cellData -> cellData.getValue().requestProperty());
        list.add(colRequest);
        return list;
    }

    public Organization(int id, String name, String address, String phone, String email, String request) {
        this.id = id;
        this.name = new SimpleStringProperty(name);
        this.address = new SimpleStringProperty(address);
        this.phone = new SimpleStringProperty(phone);
        this.email = new SimpleStringProperty(email);
        this.request = new SimpleStringProperty(request);
    }

    public String getRequest() {
        return request.get();
    }

    public void setRequest(String request) {
        this.request.set(request);
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId(){
        return id;
    }

    public String getAddress() {
        return address.get();
    }

    public void setAddress(String address) {
        this.address.set(address);
    }

    public String getEmail() {
        return email.get();
    }

    public void setEmail(String email) {
        this.email.set(email);
    }

    public String getPhone() {
        return phone.get();
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }

    public String getName() {
        return name.get();
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public StringProperty nameProperty(){
        return name;
    }

    public StringProperty addressProperty(){
        return address;
    }

    public StringProperty phoneProperty(){
        return phone;
    }

    public StringProperty emailProperty(){
        return email;
    }

    public StringProperty requestProperty(){
        return request;
    }

    @Override
    public String DBExportString() {
        String query = "INSERT into Organization ('name', 'address', 'phone', 'email', 'request') values (";
        query += "'" + getName() + "', ";
        query += "'" + getAddress() + "', ";
        query += "'" + getPhone() + "', ";
        query += "'" + getEmail() + "', ";
        query += "'" + getRequest() + "'); ";
        return query;
    }

    @Override
    public String DBFindString() {
        String query = "SELECT * from " + getEntityType().name() + " where ";
        query += "name = '" + getName() + "' and ";
        query += "address = '" + getAddress() + "' and ";
        query += "phone = '" + getPhone() + "' and ";
        query += "email = '" + getEmail() + "' and ";
        query += "request = '" + getRequest() + "'; ";
        return query;
    }

    @Override
    public String DBModifyString() {
        String query = "UPDATE Organization SET ";
        query += "name = '" + getName() + "', ";
        query += "address = '" + getAddress() + "', ";
        query += "phone = '" + getPhone() + "', ";
        query += "email = '" + getEmail() + "', ";
        query += "request = '" + getRequest() + "' where id = " + getId();
        return query;
    }

    @Override
    public void DBImportString(ResultSet dbString) {

    }

    @Override
    public EntityType getEntityType() {
        return EntityType.ORGANIZATION;
    }
}
