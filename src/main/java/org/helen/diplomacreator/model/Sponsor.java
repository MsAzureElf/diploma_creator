package org.helen.diplomacreator.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.control.TableColumn;
import org.helen.diplomacreator.utils.Logger;
import org.helen.diplomacreator.utils.StringUtils;

import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created by Helen on 30.01.2016.
 */
public class Sponsor implements DBEntity {

    private static String[] tvSponsorColNames = {"ФИО", "Должность", "Телефон", "Благодарственное письмо"};

    private int id;

    private StringProperty fio = new SimpleStringProperty("");
    private StringProperty position = new SimpleStringProperty("");
    private StringProperty phone = new SimpleStringProperty("");
    private boolean personalAgreement = false;
    private int organization = 0;
    private StringProperty thanksTemplate = new SimpleStringProperty("");

    public Sponsor() {

    }

    public Sponsor(int id, String firstName, String lastName, String fatherName, String position, String phone, int organization, String thanksTemplate) {
        this.id = id;
        this.position = new SimpleStringProperty(position);
        this.phone = new SimpleStringProperty(phone);
        this.organization = organization;
        this.fio = new SimpleStringProperty(firstName + " " + lastName + " " + fatherName);
        this.thanksTemplate = new SimpleStringProperty(thanksTemplate);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPersonalAgreement() {
        return personalAgreement;
    }

    public void setPersonalAgreement(boolean personalAgreement) {
        this.personalAgreement = personalAgreement;
    }

    public String getPhone() {
        return phone.get();
    }

    public void setPhone(String phone) {
        this.phone.set(phone);
    }

    public String getPosition() {
        return position.get();
    }

    public void setPosition(String position) {
        this.position.set(position);
    }

    public void setFio(String firstName, String lastName, String fatherName) {
        this.fio.set(firstName + " " + lastName + " " + fatherName);
    }

    public String getFatherName() {
        try {
            return StringUtils.split(fio.get(), " ").get(2);
        } catch (Exception e) {
            return "";
        }
    }

    public void setFatherName(String fatherName) {
        try {
            ArrayList<String> splittedString = StringUtils.split(fio.get(), " ");
            this.fio.set(splittedString.get(0) + " " + splittedString.get(1) + " " + fatherName);
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    public String getLastName() {
        try {
            return StringUtils.split(fio.get(), " ").get(1);
        } catch (Exception e) {
            return "";
        }
    }

    public void setLastName(String lastName) {
        try {
            ArrayList<String> splittedString = StringUtils.split(fio.get(), " ");
            this.fio.set(splittedString.get(0) + " " + lastName + " " + splittedString.get(2));
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    public String getFirstName() {
        try {
            return StringUtils.split(fio.get(), " ").get(0);
        } catch (Exception e) {
            return "";
        }
    }

    public void setFirstName(String firstName) {
        try {
            ArrayList<String> splittedString = StringUtils.split(fio.get(), " ");
            this.fio.set(firstName + " " + splittedString.get(0) + " " + splittedString.get(2));
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    public int getOrganization() {
        return organization;
    }

    public void setOrganization(int organization) {
        this.organization = organization;
    }

    public StringProperty fioProperty() {
        return fio;
    }

    public StringProperty positionProperty() {
        return position;
    }

    public StringProperty phoneProperty() {
        return phone;
    }

    public String getThanksTemplate() {
        return thanksTemplate.get();
    }

    public StringProperty thanksTemplateProperty() {
        return thanksTemplate;
    }

    public void setThanksTemplate(String thanksTemplate) {
        this.thanksTemplate.set(thanksTemplate);
    }

    public static ArrayList<TableColumn<Sponsor, String>> getColumns() {
        ArrayList<TableColumn<Sponsor, String>> list = new ArrayList<>();
        TableColumn<Sponsor, String> colFIO = new TableColumn<>(tvSponsorColNames[0]);
        colFIO.setCellValueFactory(cellData -> cellData.getValue().fioProperty());
        list.add(colFIO);
        TableColumn<Sponsor, String> colPosition = new TableColumn<>(tvSponsorColNames[1]);
        colPosition.setCellValueFactory(cellData -> cellData.getValue().positionProperty());
        list.add(colPosition);
        TableColumn<Sponsor, String> colPhone = new TableColumn<>(tvSponsorColNames[2]);
        colPhone.setCellValueFactory(cellData -> cellData.getValue().phoneProperty());
        list.add(colPhone);
        TableColumn<Sponsor, String> colThanks = new TableColumn<>(tvSponsorColNames[3]);
        colThanks.setCellValueFactory(cellData -> cellData.getValue().thanksTemplateProperty());
        list.add(colThanks);
        return list;
    }

    @Override
    public String DBExportString() {
        String query = "INSERT into Sponsor ('first_name', 'last_name', 'father_name', 'position', 'phone', 'thanks_template', 'agreement', 'organization') values (";
        query += "'" + getFirstName() + "', ";
        query += "'" + getLastName() + "', ";
        query += "'" + getFatherName() + "', ";
        query += "'" + getPosition() + "', ";
        query += "'" + getPhone() + "', ";
        query += "'" + getThanksTemplate() + "', ";
        query += "" + (isPersonalAgreement()?1:0) + ", ";
        query += "" + getOrganization() + "); ";
        return query;
    }

    @Override
    public String DBFindString() {
        String query = "SELECT * from " + getEntityType().name() + " where ";
        query += "first_name = '" + getFirstName() + "' and ";
        query += "last_name = '" + getLastName() + "' and ";
        query += "father_name = '" + getFatherName() + "' and ";
        query += "position = '" + getPosition() + "' and ";
        query += "phone = '" + getPhone() + "' and ";
        query += "thanks_template = '" + getThanksTemplate() + "' and ";
        query += "agreement = " + (isPersonalAgreement()?1:0) + " and ";
        query += "organization = " + getOrganization() + "; ";
        return query;

    }

    @Override
    public String DBModifyString() {
        String query = "UPDATE Sponsor SET ";
        query += "first_name = '" + getFirstName() + "', ";
        query += "last_name = '" + getLastName() + "', ";
        query += "father_name = '" + getFatherName() + "', ";
        query += "position = '" + getPosition() + "', ";
        query += "phone = '" + getPhone() + "', ";
        query += "thanks_template = '" + getThanksTemplate() + "', ";
        query += "agreement = " + (isPersonalAgreement()?1:0) + ", ";
        query += "organization = " + getOrganization() + " where id = " + getId();
        return query;
    }

    @Override
    public void DBImportString(ResultSet dbString) {

    }

    @Override
    public EntityType getEntityType() {
        return EntityType.SPONSOR;
    }
}
