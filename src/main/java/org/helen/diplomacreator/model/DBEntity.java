package org.helen.diplomacreator.model;

import java.sql.ResultSet;

/**
 * Created by Helen on 06.02.2016.
 */
public interface DBEntity {

    String DBExportString();

    String DBFindString();

    String DBModifyString();

    public void DBImportString(ResultSet dbString);

    public EntityType getEntityType();

    int getId();

    void setId(int id);

    enum EntityType {
        DIPLOMA("Diploma"),
        MENTOR("Mentor"),
        ORGANIZATION("Organization"),
        PARTICIPANT("Participant"),
        POSITION("Position"),
        SPONSOR("Sponsor"),
        SUBJECT("Subject");

        private final String value;

        EntityType(String value) {
            this.value = value;
        }

        static public EntityType getEntityType(String entityType) {
            for (EntityType type : EntityType.values()) {
                if (type.toString().equalsIgnoreCase(entityType)) {
                    return type;
                }
            }
            return DIPLOMA;
        }

        public String getValue(){
            return value;
        }

    }

}
