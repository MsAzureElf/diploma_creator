package org.helen.diplomacreator.logic;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.helen.diplomacreator.model.*;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by Helen on 14.02.2016.
 */
public class SponsorController implements FormController {

    @FXML
    private TextField tfFirstName;
    @FXML
    private TextField tfLastName;
    @FXML
    private TextField tfFatherName;
    @FXML
    private TextField tfPhone;
    @FXML
    private ComboBox<String> cbPosition;
    @FXML
    public ComboBox<String> cbDiploma;

    @FXML
    private Button okBtn;
    @FXML
    private Button cancelBtn;
    @FXML
    private Button deleteBtn;

    private Sponsor data;
    private DialogResult dialogResult = DialogResult.CANCEL;
    private Stage stage;

    @FXML
    private void initialize() {
        okBtn.setOnAction(event -> {
            data.setFio(tfFirstName.getText(), tfLastName.getText(), tfFatherName.getText());
            data.setPhone(tfPhone.getText());
            data.setPosition(cbPosition.getValue());
            data.setThanksTemplate(cbDiploma.getValue());
            dialogResult = DialogResult.OK;
            stage.close();
        });
        cancelBtn.setOnAction(event -> {
            dialogResult = DialogResult.CANCEL;
            stage.close();
        });
        deleteBtn.setOnAction(event -> {
            dialogResult = DialogResult.DELETE;
            stage.close();
        });
        ArrayList<DBEntity> dipList = DBConnector.getInstance().readAll(DBEntity.EntityType.DIPLOMA);
        for (DBEntity entity : dipList) {
            cbDiploma.getItems().add(((Diploma) entity).getCompetitionName());
        }
        ObservableList<String> list = FXCollections.observableArrayList();
        ArrayList<DBEntity> positions = DBConnector.getInstance().readAll(DBEntity.EntityType.POSITION);
        list.addAll(positions.stream().map(entity -> ((Position) entity).getFullName()).collect(Collectors.toList()));
        cbPosition.setItems(list);
    }

    @Override
    public void setData(DBEntity sponsorData) {
        if (!(sponsorData instanceof Sponsor))
            this.data = new Sponsor();
        else
            this.data = (Sponsor) sponsorData;
        tfFirstName.setText(data.getFirstName());
        tfLastName.setText(data.getLastName());
        tfFatherName.setText(data.getFatherName());
        tfPhone.setText(data.getPhone());
        cbPosition.setValue(data.getPosition());
        cbDiploma.setValue(data.getThanksTemplate());
    }

    @Override
    public DialogResult getDialogResult() {
        return dialogResult;
    }

    @Override
    public DBEntity getData() {
        return data;
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
