package org.helen.diplomacreator.logic;

import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.helen.diplomacreator.utils.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

/**
 * Created by Helen on 14.02.2016.
 */
public class DiplomaController {

    @FXML
    public BorderPane borderPane;
    @FXML
    public TextField tfHeaderX;
    @FXML
    public TextField tfHeaderY;
    @FXML
    public Button okBtn;
    @FXML
    public Button cancelBtn;
    @FXML
    private ImageView preView;
    @FXML
    private Button diplomaLoadBtn;
    @FXML
    private TextField tfDiploma;

    private BufferedImage template = null;

    private FormController.DialogResult dialogResult = FormController.DialogResult.CANCEL;

    private Stage stage;

    @FXML
    private void initialize() {
        borderPane.getStyleClass().addAll("grid");
        preView.getStyleClass().addAll("grid");
        diplomaLoadBtn.setOnAction(event -> {
            FileChooser c = new FileChooser();
            File file = c.showOpenDialog(null);
            if (file == null)
                return;
            tfDiploma.setText(file.getAbsolutePath());
            reloadImage(tfDiploma.getText());
        });
        preView.setOnMousePressed(event -> {
            if (preView.getImage() == null)
                return;
            int percentX = (int) (100 * event.getX() / preView.getBoundsInParent().getWidth());
            int percentY = (int) (100 * event.getY() / preView.getBoundsInParent().getHeight());
            tfHeaderX.setText(Integer.toString(percentX));
            tfHeaderY.setText(Integer.toString(percentY));
            prepareImage();
        });
        tfHeaderX.setDisable(true);
        tfHeaderY.setDisable(true);
        okBtn.setOnAction(event -> {
            dialogResult = FormController.DialogResult.OK;
            stage.close();
        });
        cancelBtn.setOnAction(event -> {
            dialogResult = FormController.DialogResult.CANCEL;
            stage.close();
        });
    }

    private void reloadImage(String file) {
        template = null;
        try {
            template = ImageIO.read(new File(file));
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
            return;
        }
        prepareImage();
    }

    private void prepareImage() {
        if (template == null)
            return;
        int scaledWidth = (int) preView.getBoundsInParent().getWidth();
        int scaledHeight = (int) (template.getHeight() * preView.getBoundsInParent().getWidth() / template.getWidth());
        BufferedImage image = new BufferedImage(scaledWidth, scaledHeight, template.getType());
        Graphics graphics = image.getGraphics();
        graphics.drawImage(template, 0, 0, scaledWidth, scaledHeight, null);
        graphics.setColor(Color.red);
        graphics.setFont(new Font("Times New Roman", Font.PLAIN, 16));
        graphics.fillOval(scaledWidth * Integer.parseInt(tfHeaderX.getText()) / 100, scaledHeight * Integer.parseInt(tfHeaderY.getText()) / 100,
                5, 5);
        setImage(image);
    }

    public void setImage(BufferedImage image) {
        Image img = SwingFXUtils.toFXImage(image, new WritableImage(image.getWidth(), image.getHeight()));
        preView.setImage(img);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public String getDiplomaPath() {
        return tfDiploma.getText();
    }

    public int getX() {
        return Integer.parseInt(tfHeaderX.getText());
    }

    public int getY() {
        return Integer.parseInt(tfHeaderY.getText());
    }

    public void setDiplomaPath(String s) {
        tfDiploma.setText(s);
        if (!s.isEmpty())
            reloadImage(tfDiploma.getText());
    }

    public void setX(int x) {
        tfHeaderX.setText(Integer.toString(x));
        prepareImage();
    }

    public void setY(int y) {
        tfHeaderY.setText(Integer.toString(y));
        prepareImage();
    }

    public FormController.DialogResult getDialogResult() {
        return dialogResult;
    }

}
