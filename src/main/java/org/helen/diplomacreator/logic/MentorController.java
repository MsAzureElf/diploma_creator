package org.helen.diplomacreator.logic;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.helen.diplomacreator.model.*;
import org.helen.diplomacreator.utils.StringUtils;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Created by Helen on 14.02.2016.
 */
public class MentorController implements FormController {

    @FXML
    private TextField tfFirstName;
    @FXML
    private TextField tfLastName;
    @FXML
    private TextField tfFatherName;
    @FXML
    public ComboBox<String> cbDiploma;
    @FXML
    private ComboBox<String> cbPosition;

    @FXML
    private Button okBtn;
    @FXML
    private Button cancelBtn;
    @FXML
    private Button deleteBtn;

    private Mentor data;
    private DialogResult dialogResult = DialogResult.CANCEL;
    private Stage stage;

    @FXML
    private void initialize() {
        okBtn.setOnAction(event -> {
            String name = tfFirstName.getText()+" "+tfLastName.getText();
            if (!tfFatherName.getText().isEmpty())
                name += " "+tfFatherName.getText();
            data.setName(name);
            data.setThanksTemplate(cbDiploma.getValue());
            data.setPosition(cbPosition.getValue());
            dialogResult = DialogResult.OK;
            stage.close();
        });
        cancelBtn.setOnAction(event -> {
            dialogResult = DialogResult.CANCEL;
            stage.close();
        });
        deleteBtn.setOnAction(event -> {
            dialogResult = DialogResult.DELETE;
            stage.close();
        });
        ArrayList<DBEntity> dipList = DBConnector.getInstance().readAll(DBEntity.EntityType.DIPLOMA);
        for (DBEntity entity : dipList) {
            cbDiploma.getItems().add(((Diploma) entity).getCompetitionName());
        }
        ObservableList<String> list = FXCollections.observableArrayList();
        ArrayList<DBEntity> positions = DBConnector.getInstance().readAll(DBEntity.EntityType.POSITION);
        list.addAll(positions.stream().map(entity -> ((Position) entity).getFullName()).collect(Collectors.toList()));
        cbPosition.setItems(list);
    }

    @Override
    public void setData(DBEntity mentorData) {
        if (!(mentorData instanceof Mentor))
            this.data = new Mentor();
        else
            this.data = (Mentor) mentorData;
        ArrayList<String> name = StringUtils.split(data.getName(), " ");
        if (name != null && name.size() > 0)
            tfFirstName.setText(name.get(0));
        else
            tfFirstName.setText("");
        if (name != null && name.size() > 1)
            tfLastName.setText(name.get(1));
        else
            tfLastName.setText("");
        if (name != null && name.size() > 2)
            tfFatherName.setText(name.get(2));
        else
            tfFatherName.setText("");
        cbDiploma.setValue(data.getThanksTemplate());
        cbPosition.setValue(data.getPosition());
    }

    @Override
    public DialogResult getDialogResult() {
        return dialogResult;
    }

    @Override
    public DBEntity getData() {
        return data;
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
