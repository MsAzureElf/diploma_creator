package org.helen.diplomacreator.logic;

import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.helen.diplomacreator.model.*;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xssf.extractor.XSSFExcelExtractor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.helen.diplomacreator.utils.Logger;
import org.helen.diplomacreator.utils.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Helen on 30.01.2016.
 */
public class MainController {

    private static MainController instance;
    private Stage stage;

    @FXML
    private VBox mainPane;

    @FXML
    public TabPane tabPane;
    @FXML
    public Button btnSearch;
    @FXML
    public Button btnClear;
    @FXML
    public Button btnAdd;
    @FXML
    public GridPane searchPane;
    @FXML
    public TextField tfFindOrganization;

    @FXML
    private Button loadBtn;
    @FXML
    private Button printBtn;
    @FXML
    private Button statisticsBtn;
    @FXML
    private Button findBtn;
    @FXML
    private Button reportBtn;
    @FXML
    private ListView<String> lvSponsorOrganizations;
    @FXML
    private ListView<String> lvMentorOrganizations;
    @FXML
    private ListView<String> lvParticipantOrganizations;
    @FXML
    private ListView<String> lvSubjects;
    @FXML
    private ListView<String> lvPositions;
    @FXML
    private ListView<String> lvDiplomas;
    @FXML
    private TableView<Organization> tvOrganizations;
    @FXML
    private TableView<Sponsor> tvSponsors;
    @FXML
    private TableView<Mentor> tvMentors;
    @FXML
    private TableView<Participant> tvParticipants;

    @FXML
    private SplitPane spMentors;
    @FXML
    private SplitPane spParticipants;
    @FXML
    private SplitPane spSponsors;
    @FXML
    private SplitPane spSubjects;
    @FXML
    private SplitPane spPositions;
    @FXML
    private SplitPane spDiplomas;

    @FXML
    private TextField tfSubjectFullName;
    @FXML
    private TextField tfSubjectShortName;
    @FXML
    private TextField tfPositionFullName;
    @FXML
    private TextField tfPositionShortName;
    @FXML
    private TextField tfDiplomaFullName;
    @FXML
    public TextField tfCompetitionName;
    @FXML
    public TextField tfCompetitionType;
    @FXML
    private Button diplomaTemplateBtn;
    @FXML
    private Button diplomaSaveBtn;
    @FXML
    private Button diplomaAddBtn;
    @FXML
    public Button subjectSaveBtn;
    @FXML
    public Button subjectAddBtn;
    @FXML
    public Button positionSaveBtn;
    @FXML
    public Button positionAddBtn;
    @FXML
    public TextArea taThanksTextMentor;
    @FXML
    public TextArea taThanksTextSponsor;
    @FXML
    public RadioButton rbCompetition;
    @FXML
    public RadioButton rbOlympiad;

    private Diploma currentDiploma = new Diploma();

    private ObservableList<Participant> participantList = FXCollections.observableArrayList();
    private ObservableList<Sponsor> sponsorList = FXCollections.observableArrayList();
    private ObservableList<Mentor> mentorList = FXCollections.observableArrayList();

    private List<File> loadFiles = null;
    private String selectedDiplomaForRequest;

    private Service serviceLoad = new Service() {
        @Override
        protected Task createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() {
                    try {
                        if (loadFiles == null)
                            return null;
                        for (File file : loadFiles) {
                            parseExcel(file);
                        }
                        loadFiles = null;
                    } catch (Exception e) {
                        e.printStackTrace(Logger.getInstance().getStream());
                    }
                    return null;
                }
            };
        }
    };
    private Service servicePrint = new Service() {
        @Override
        protected Task createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    HashMap<String, DiplomaPrinter> map = new HashMap<>();
                    DiplomaPrinter defaultPrinter = new DiplomaPrinter(currentDiploma);
                    for (Participant participant : participantList) {
                        if (participant.getDiploma().isEmpty())
                            defaultPrinter.createDiploma(participant);
                        else {
                            String diploma = participant.getDiploma();
                            if (map.containsKey(diploma))
                                map.get(diploma).createDiploma(participant);
                            else {
                                DBEntity entity = DBConnector.getInstance().find(DBEntity.EntityType.DIPLOMA, "competition_name", diploma);
                                if (entity == null)
                                    continue;
                                DiplomaPrinter printer = new DiplomaPrinter((Diploma) entity);
                                map.put(diploma, printer);
                                printer.createDiploma(participant);
                            }
                        }
                    }
                    for (Sponsor sponsor : sponsorList) {
                        if (sponsor.getThanksTemplate().isEmpty())
                            defaultPrinter.createThanksLetter(sponsor);
                        else {
                            String thanksLetter = sponsor.getThanksTemplate();
                            if (map.containsKey(thanksLetter))
                                map.get(thanksLetter).createThanksLetter(sponsor);
                            else {
                                DBEntity entity = DBConnector.getInstance().find(DBEntity.EntityType.DIPLOMA, "competition_name", thanksLetter);
                                if (entity == null)
                                    continue;
                                DiplomaPrinter printer = new DiplomaPrinter((Diploma) entity);
                                map.put(thanksLetter, printer);
                                printer.createThanksLetter(sponsor);
                            }
                        }
                    }
                    for (Mentor mentor : mentorList) {
                        if (mentor.getThanksTemplate().isEmpty())
                            defaultPrinter.createThanksLetter(mentor);
                        else {
                            String thanksLetter = mentor.getThanksTemplate();
                            if (map.containsKey(thanksLetter))
                                map.get(thanksLetter).createThanksLetter(mentor);
                            else {
                                DBEntity entity = DBConnector.getInstance().find(DBEntity.EntityType.DIPLOMA, "competition_name", thanksLetter);
                                if (entity == null)
                                    continue;
                                DiplomaPrinter printer = new DiplomaPrinter((Diploma) entity);
                                map.put(thanksLetter, printer);
                                printer.createThanksLetter(mentor);
                            }
                        }
                    }
                    return null;
                }
            };

        }
    };
    private Service serviceReport = new Service() {
        @Override
        protected Task createTask() {
            return new Task<Void>() {
                @Override
                protected Void call() {
                    return null;
                }
            };
        }
    };

    @FXML
    private void initialize() {
        instance = this;
        DBConnector.getInstance();
        loadBtn.setOnAction(event -> {
            String selected = getSelectedDiploma("");
            if (selected.isEmpty())
                return;
            selectedDiplomaForRequest = selected;
            FileChooser c = new FileChooser();
            List<File> files = c.showOpenMultipleDialog(null);
            if (files == null)
                return;
            loadFiles = files;
            serviceLoad.restart();
        });
        printBtn.setOnAction(event -> {
            PrintSettingsController.PrintMode mode = openPrintModeDialog();
            if (mode == PrintSettingsController.PrintMode.NONE)
                return;
            participantList.clear();
            sponsorList.clear();
            mentorList.clear();
            switch (mode) {
                case PRINT_ORGANIZATION: {
                    int selectedOrganization = tvOrganizations.getSelectionModel().getSelectedIndex();
                    if (selectedOrganization == -1)
                        return;
                    ArrayList<DBEntity> list = DBConnector.getInstance().read(DBEntity.EntityType.PARTICIPANT, "organization", selectedOrganization + 1, false);
                    participantList.addAll(list.stream().map(entity -> (Participant) entity).collect(Collectors.toList()));
                    list = DBConnector.getInstance().read(DBEntity.EntityType.SPONSOR, "organization", selectedOrganization + 1, false);
                    sponsorList.addAll(list.stream().map(entity -> (Sponsor) entity).collect(Collectors.toList()));
                    list = DBConnector.getInstance().read(DBEntity.EntityType.MENTOR, "organization", selectedOrganization + 1, false);
                    mentorList.addAll(list.stream().map(entity -> (Mentor) entity).collect(Collectors.toList()));
                    break;
                }
                case PRINT_ALL: {
                    ArrayList<DBEntity> list = DBConnector.getInstance().readAll(DBEntity.EntityType.PARTICIPANT);
                    participantList.addAll(list.stream().map(entity -> (Participant) entity).collect(Collectors.toList()));
                    list = DBConnector.getInstance().readAll(DBEntity.EntityType.SPONSOR);
                    sponsorList.addAll(list.stream().map(entity -> (Sponsor) entity).collect(Collectors.toList()));
                    list = DBConnector.getInstance().readAll(DBEntity.EntityType.MENTOR);
                    mentorList.addAll(list.stream().map(entity -> (Mentor) entity).collect(Collectors.toList()));
                    break;
                }
                case PRINT_MENTOR: {
                    int selIndex = tvMentors.getSelectionModel().getSelectedIndex();
                    if (selIndex == -1)
                        return;
                    ArrayList<DBEntity> list = DBConnector.getInstance().read(DBEntity.EntityType.MENTOR, "id", tvMentors.getItems().get(selIndex).getId(), false);
                    mentorList.addAll(list.stream().map(entity -> (Mentor) entity).collect(Collectors.toList()));
                    break;
                }
                case PRINT_PARTICIPANT: {
                    int selIndex = tvParticipants.getSelectionModel().getSelectedIndex();
                    if (selIndex == -1)
                        return;
                    ArrayList<DBEntity> list = DBConnector.getInstance().read(DBEntity.EntityType.PARTICIPANT, "id", tvParticipants.getItems().get(selIndex).getId(), false);
                    participantList.addAll(list.stream().map(entity -> (Participant) entity).collect(Collectors.toList()));
                    break;
                }
                case PRINT_SPONSOR: {
                    int selIndex = tvSponsors.getSelectionModel().getSelectedIndex();
                    if (selIndex == -1)
                        return;
                    ArrayList<DBEntity> list = DBConnector.getInstance().read(DBEntity.EntityType.SPONSOR, "id", tvSponsors.getItems().get(selIndex).getId(), false);
                    sponsorList.addAll(list.stream().map(entity -> (Sponsor) entity).collect(Collectors.toList()));
                    break;
                }
            }
            servicePrint.restart();
        });
        statisticsBtn.setOnAction(event -> {
            FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/main/org.helen.diplomacreator.view/Statistics.fxml"));
            Stage dialogStage = new Stage();
            Pane page = null;
            try {
                page = loader.load();
            } catch (Exception e) {
                e.printStackTrace(Logger.getInstance().getStream());
            }
            Scene scene = new Scene(page);
            String css = this.getClass().getResource("/main/org.helen.diplomacreator.view/main.css").toExternalForm();
            StatisticsController controller = loader.getController();
            controller.setStage(dialogStage);
            scene.getStylesheets().add(css);
            dialogStage.setScene(scene);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.showAndWait();
        });
        reportBtn.setOnAction(event1 -> {
            String diploma = StringUtils.replaceForbidden(getSelectedDiploma("Выберите диплома для отчета"), DiplomaPrinter.forbiddenCharacters);
            if (diploma.isEmpty())
                return;
            ReportPrinter reportPrinter = new ReportPrinter();
            reportPrinter.createReport(diploma);
        });
        //searchPane.getStyleClass().addAll("grid");
        serviceLoad.setOnSucceeded(event -> reloadTable(DBEntity.EntityType.ORGANIZATION, 0));
        mainPane.cursorProperty().bind(Bindings.when(serviceLoad.runningProperty().or(servicePrint.runningProperty()).or(serviceReport.runningProperty())).then(Cursor.WAIT).otherwise(Cursor.DEFAULT));
        loadBtn.disableProperty().bind(serviceLoad.runningProperty());
        printBtn.disableProperty().bind(servicePrint.runningProperty());
        reportBtn.disableProperty().bind(serviceReport.runningProperty());
        tvOrganizations.getColumns().addAll(Organization.getColumns());
        tvOrganizations.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tvOrganizations.setRowFactory(new RowCallback<>());
        tvSponsors.getColumns().addAll(Sponsor.getColumns());
        tvSponsors.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tvSponsors.setRowFactory(new RowCallback<>());
        tvMentors.getColumns().addAll(Mentor.getColumns());
        tvMentors.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tvMentors.setRowFactory(new RowCallback<>());
        tvParticipants.getColumns().addAll(Participant.getColumns());
        tvParticipants.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        tvParticipants.setRowFactory(new RowCallback<>());
        spMentors.setOrientation(Orientation.HORIZONTAL);
        spMentors.setDividerPosition(0, 0.2);
        spParticipants.setOrientation(Orientation.HORIZONTAL);
        spParticipants.setDividerPosition(0, 0.2);
        spSponsors.setOrientation(Orientation.HORIZONTAL);
        spSponsors.setDividerPosition(0, 0.2);
        spSubjects.setOrientation(Orientation.HORIZONTAL);
        spSubjects.setDividerPosition(0, 0.2);
        spPositions.setOrientation(Orientation.HORIZONTAL);
        spPositions.setDividerPosition(0, 0.2);
        spDiplomas.setOrientation(Orientation.HORIZONTAL);
        spDiplomas.setDividerPosition(0, 0.2);
        lvParticipantOrganizations.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lvParticipantOrganizations.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            try {
                int index = lvParticipantOrganizations.getSelectionModel().getSelectedIndex();
                if (index != -1) {
                    reloadTable(DBEntity.EntityType.PARTICIPANT, index);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        lvSponsorOrganizations.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lvSponsorOrganizations.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            int index = lvSponsorOrganizations.getSelectionModel().getSelectedIndex();
            if (index != -1) {
                reloadTable(DBEntity.EntityType.SPONSOR, index);
            }
        });
        lvMentorOrganizations.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lvMentorOrganizations.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            int index = lvMentorOrganizations.getSelectionModel().getSelectedIndex();
            if (index != -1) {
                /*DBEntity entity = DBConnector.getInstance().find(DBEntity.EntityType.ORGANIZATION, "name",
                        lvMentorOrganizations.getSelectionModel().getSelectedItem());
                if (entity == null)
                    return;*/
                reloadTable(DBEntity.EntityType.MENTOR, index);
            }
        });
        lvPositions.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lvPositions.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            int index = lvPositions.getSelectionModel().getSelectedIndex();
            if (index != -1)
                reloadTable(DBEntity.EntityType.POSITION, index);
        });
        lvSubjects.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lvSubjects.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            int index = lvSubjects.getSelectionModel().getSelectedIndex();
            if (index != -1)
                reloadTable(DBEntity.EntityType.SUBJECT, index);
        });
        lvDiplomas.getSelectionModel().setSelectionMode(SelectionMode.SINGLE);
        lvDiplomas.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            int index = lvDiplomas.getSelectionModel().getSelectedIndex();
            if (index != -1)
                reloadTable(DBEntity.EntityType.DIPLOMA, index);
        });
        ToggleGroup group = new ToggleGroup();
        rbCompetition.setToggleGroup(group);
        rbOlympiad.setToggleGroup(group);
        diplomaTemplateBtn.setOnAction(event -> {
            FXMLLoader loader = new FXMLLoader(this.getClass().getResource(RowCallback.getFxmlLocation(DBEntity.EntityType.DIPLOMA)));
            Stage dialogStage = new Stage();
            Pane page = null;
            try {
                page = loader.load();
            } catch (Exception e) {
                e.printStackTrace(Logger.getInstance().getStream());
            }
            Scene scene = new Scene(page);
            String css = this.getClass().getResource("/main/org.helen.diplomacreator.view/main.css").toExternalForm();
            DiplomaController controller = loader.getController();
            controller.setDiplomaPath(currentDiploma.getDiplomaTemplate());
            controller.setX(currentDiploma.getX());
            controller.setY(currentDiploma.getY());
            controller.setStage(dialogStage);
            scene.getStylesheets().add(css);
            dialogStage.setScene(scene);
            dialogStage.initModality(Modality.APPLICATION_MODAL);
            dialogStage.showAndWait();
            if (controller.getDialogResult() == FormController.DialogResult.OK) {
                currentDiploma.setDiplomaTemplate(controller.getDiplomaPath());
                currentDiploma.setX(controller.getX());
                currentDiploma.setY(controller.getY());
                currentDiploma.setThanksTextMentor(taThanksTextMentor.getText());
                currentDiploma.setThanksTextSponsor(taThanksTextSponsor.getText());
                currentDiploma.setOlympiad(rbOlympiad.isSelected() ? true : false);
            }
        });
        diplomaAddBtn.setOnAction(event -> {
            currentDiploma.setFullName(tfDiplomaFullName.getText());
            currentDiploma.setCompetitionName(tfCompetitionName.getText());
            currentDiploma.setCompetitionType(tfCompetitionType.getText());
            currentDiploma.setThanksTextMentor(taThanksTextMentor.getText());
            currentDiploma.setThanksTextSponsor(taThanksTextSponsor.getText());
            currentDiploma.setOlympiad(rbOlympiad.isSelected() ? true : false);
            DBConnector.getInstance().write(currentDiploma);
            reloadTable(DBEntity.EntityType.DIPLOMA, 0);
        });
        diplomaSaveBtn.setOnAction(event -> {
            currentDiploma.setFullName(tfDiplomaFullName.getText());
            currentDiploma.setCompetitionName(tfCompetitionName.getText());
            currentDiploma.setCompetitionType(tfCompetitionType.getText());
            currentDiploma.setThanksTextMentor(taThanksTextMentor.getText());
            currentDiploma.setThanksTextSponsor(taThanksTextSponsor.getText());
            currentDiploma.setOlympiad(rbOlympiad.isSelected() ? true : false);
            DBConnector.getInstance().update(currentDiploma);
            reloadTable(DBEntity.EntityType.DIPLOMA, getSelectedLVIndex(DBEntity.EntityType.DIPLOMA));
        });
        subjectAddBtn.setOnAction(event -> {
            DBConnector.getInstance().write(new Subject(0, tfSubjectShortName.getText(), tfSubjectFullName.getText()));
            reloadTable(DBEntity.EntityType.SUBJECT, 0);
        });
        subjectSaveBtn.setOnAction(event -> {
            if (lvSubjects.getSelectionModel().getSelectedIndex() == -1)
                return;
            String name = lvSubjects.getSelectionModel().getSelectedItem();
            Subject subject = (Subject) DBConnector.getInstance().find(DBEntity.EntityType.SUBJECT, "name", name);
            subject.setFullName(tfSubjectFullName.getText());
            subject.setShortName(tfSubjectShortName.getText());
            DBConnector.getInstance().update(subject);
            reloadTable(DBEntity.EntityType.SUBJECT, getSelectedLVIndex(DBEntity.EntityType.SUBJECT));
        });
        positionAddBtn.setOnAction(event -> {
            DBConnector.getInstance().write(new Position(0, tfPositionShortName.getText(), tfPositionFullName.getText()));
            reloadTable(DBEntity.EntityType.POSITION, 0);
        });
        positionSaveBtn.setOnAction(event -> {
            if (lvPositions.getSelectionModel().getSelectedIndex() == -1)
                return;
            String name = lvPositions.getSelectionModel().getSelectedItem();
            Position position = (Position) DBConnector.getInstance().find(DBEntity.EntityType.POSITION, "name", name);
            position.setFullName(tfPositionFullName.getText());
            position.setShortName(tfPositionShortName.getText());
            DBConnector.getInstance().update(position);
            reloadTable(DBEntity.EntityType.POSITION, getSelectedLVIndex(DBEntity.EntityType.POSITION));
        });
        reloadTable(DBEntity.EntityType.ORGANIZATION, 0);
        reloadTable(DBEntity.EntityType.DIPLOMA, 0);
        tfDiplomaFullName.setText(currentDiploma.getFullName());
        tfCompetitionName.setText(currentDiploma.getCompetitionName());
        tfCompetitionType.setText(currentDiploma.getCompetitionType());
        if (currentDiploma.isOlympiad())
            rbOlympiad.setSelected(true);
        else
            rbCompetition.setSelected(true);
        taThanksTextMentor.setText(currentDiploma.getThanksTextMentor());
        taThanksTextSponsor.setText(currentDiploma.getThanksTextSponsor());
        tfFindOrganization.setOnAction(event -> {
            reloadTable(DBEntity.EntityType.ORGANIZATION, 0);
        });
        btnClear.setOnAction(event -> {
            tfFindOrganization.setText("");
            tfFindOrganization.fireEvent(new ActionEvent());
        });
        btnSearch.setOnAction(event -> tfFindOrganization.fireEvent(new ActionEvent()));
        btnAdd.setOnAction(event -> {
            DBEntity entity;
            String organization = "";
            switch (tabPane.getSelectionModel().getSelectedIndex()) {
                case 1: {
                    if (lvSponsorOrganizations.getSelectionModel().getSelectedIndex() != -1)
                        organization = lvSponsorOrganizations.getSelectionModel().getSelectedItem();
                    entity = new Sponsor();
                    DBEntity org = DBConnector.getInstance().find(DBEntity.EntityType.ORGANIZATION, "name", organization);
                    if (org != null) {
                        ((Sponsor) entity).setOrganization(org.getId());
                    } else
                        return;
                    break;
                }
                case 2: {
                    if (lvMentorOrganizations.getSelectionModel().getSelectedIndex() != -1)
                        organization = lvMentorOrganizations.getSelectionModel().getSelectedItem();
                    entity = new Mentor();
                    DBEntity org = DBConnector.getInstance().find(DBEntity.EntityType.ORGANIZATION, "name", organization);
                    if (org != null) {
                        ((Mentor) entity).setOrganization(org.getId());
                    } else
                        return;
                }
                break;
                case 3: {
                    if (lvParticipantOrganizations.getSelectionModel().getSelectedIndex() != -1)
                        organization = lvParticipantOrganizations.getSelectionModel().getSelectedItem();
                    entity = new Participant();
                    DBEntity org = DBConnector.getInstance().find(DBEntity.EntityType.ORGANIZATION, "name", organization);
                    if (org != null) {
                        ((Participant) entity).setOrganization(org.getId());
                    } else
                        return;
                    break;
                }
                default:
                    return;
            }
            String fxmlLocation = RowCallback.getFxmlLocation(entity.getEntityType());
            if (fxmlLocation == null)
                return;
            RowCallback.openForm(entity, fxmlLocation);
            DBConnector.getInstance().write(entity);
            reloadTable(entity.getEntityType(), getSelectedLVIndex(entity.getEntityType()));
        });
    }

    public void reloadTable(DBEntity.EntityType entityType, int index) {
        try {
            switch (entityType) {
                case ORGANIZATION: {
                    tvOrganizations.getItems().clear();
                    lvParticipantOrganizations.getItems().clear();
                    lvMentorOrganizations.getItems().clear();
                    lvSponsorOrganizations.getItems().clear();
                    lvSubjects.getItems().clear();
                    lvPositions.getItems().clear();
                    ArrayList<DBEntity> list = DBConnector.getInstance().read(DBEntity.EntityType.ORGANIZATION, "name", tfFindOrganization.getText(), true);
                    for (DBEntity entity : list) {
                        tvOrganizations.getItems().add((Organization) entity);
                        lvMentorOrganizations.getItems().add(((Organization) entity).getName());
                        lvSponsorOrganizations.getItems().add(((Organization) entity).getName());
                        lvParticipantOrganizations.getItems().add(((Organization) entity).getName());
                    }
                    reloadTable(DBEntity.EntityType.PARTICIPANT, index);
                    reloadTable(DBEntity.EntityType.MENTOR, index);
                    reloadTable(DBEntity.EntityType.SPONSOR, index);
                    reloadTable(DBEntity.EntityType.POSITION, 0);
                    reloadTable(DBEntity.EntityType.SUBJECT, 0);
                    break;
                }
                case PARTICIPANT: {
                    if (lvParticipantOrganizations.getItems().size() > index)
                        lvParticipantOrganizations.getSelectionModel().select(index);
                    tvParticipants.getItems().clear();
                    if (lvParticipantOrganizations.getItems().size() <= index)
                        return;
                    DBEntity org = DBConnector.getInstance().find(DBEntity.EntityType.ORGANIZATION, "name", lvParticipantOrganizations.getItems().get(index));
                    if (org == null)
                        return;
                    ArrayList<DBEntity> list = DBConnector.getInstance().read(DBEntity.EntityType.PARTICIPANT, "organization", org.getId(), false);
                    for (DBEntity entity : list)
                        tvParticipants.getItems().add((Participant) entity);
                    break;
                }
                case MENTOR: {
                    if (lvMentorOrganizations.getItems().size() > index)
                        lvMentorOrganizations.getSelectionModel().select(index);
                    tvMentors.getItems().clear();
                    if (lvMentorOrganizations.getItems().size() <= index)
                        return;
                    DBEntity org = DBConnector.getInstance().find(DBEntity.EntityType.ORGANIZATION, "name", lvMentorOrganizations.getItems().get(index));
                    if (org == null)
                        return;
                    ArrayList<DBEntity> list = DBConnector.getInstance().read(DBEntity.EntityType.MENTOR, "organization", org.getId(), false);
                    for (DBEntity entity : list)
                        tvMentors.getItems().add((Mentor) entity);
                    break;
                }
                case SPONSOR: {
                    if (lvSponsorOrganizations.getItems().size() > index)
                        lvSponsorOrganizations.getSelectionModel().select(index);
                    tvSponsors.getItems().clear();
                    if (lvSponsorOrganizations.getItems().size() <= index)
                        return;
                    DBEntity org = DBConnector.getInstance().find(DBEntity.EntityType.ORGANIZATION, "name",
                            lvSponsorOrganizations.getSelectionModel().getSelectedItem());
                    if (org == null)
                        return;
                    ArrayList<DBEntity> list = DBConnector.getInstance().read(DBEntity.EntityType.SPONSOR, "organization", org.getId(), false);
                    for (DBEntity entity : list)
                        tvSponsors.getItems().add((Sponsor) entity);
                    break;
                }
                case DIPLOMA: {
                    ArrayList<DBEntity> list = DBConnector.getInstance().readAll(entityType);
                    updateList(lvDiplomas, list, index);
                    if (list.size() > index) {
                        currentDiploma = (Diploma) list.get(index);
                        tfDiplomaFullName.setText(currentDiploma.getFullName());
                        tfCompetitionName.setText(currentDiploma.getCompetitionName());
                        tfCompetitionType.setText(currentDiploma.getCompetitionType());
                        if (currentDiploma.isOlympiad())
                            rbOlympiad.setSelected(true);
                        else
                            rbCompetition.setSelected(true);
                        taThanksTextMentor.setText(currentDiploma.getThanksTextMentor());
                        taThanksTextSponsor.setText(currentDiploma.getThanksTextSponsor());
                    }
                    break;
                }
                case POSITION: {
                    ArrayList<DBEntity> list = DBConnector.getInstance().readAll(entityType);
                    updateList(lvPositions, list, index);
                    if (list.size() > index) {
                        tfPositionFullName.setText(((Position) list.get(index)).getFullName());
                        tfPositionShortName.setText(((Position) list.get(index)).getShortName());
                    }
                    break;
                }
                case SUBJECT: {
                    ArrayList<DBEntity> list = DBConnector.getInstance().readAll(entityType);
                    updateList(lvSubjects, list, index);
                    if (list.size() > index) {
                        tfSubjectFullName.setText(((Subject) list.get(index)).getFullName());
                        tfSubjectShortName.setText(((Subject) list.get(index)).getShortName());
                    }
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    private String getSelectedDiploma(String caption) {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/main/org.helen.diplomacreator.view/DiplomaSelector.fxml"));
        Stage dialogStage = new Stage();
        Pane page = null;
        try {
            page = loader.load();
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
        Scene scene = new Scene(page);
        String css = this.getClass().getResource("/main/org.helen.diplomacreator.view/main.css").toExternalForm();
        DiplomaSelectorController controller = loader.getController();
        controller.setStage(dialogStage);
        if (!caption.isEmpty())
            controller.setCaption(caption);
        scene.getStylesheets().add(css);
        dialogStage.setScene(scene);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.showAndWait();
        if (controller.getDialogResult() == FormController.DialogResult.CANCEL)
            return "";
        return controller.getSelectedDiploma();
    }

    private void updateList(ListView<String> listView, ArrayList<DBEntity> list, int index) {
        boolean newRec = false;
        if (list.size() != listView.getItems().size())
            newRec = true;
        else {
            for (int i = 0; i < list.size(); ++i) {
                boolean check = false;
                if (list.get(i) instanceof Subject)
                    check = !((Subject) list.get(i)).getFullName().contentEquals(listView.getItems().get(i));
                else if (list.get(i) instanceof Position)
                    check = !((Position) list.get(i)).getFullName().contentEquals(listView.getItems().get(i));
                else if (list.get(i) instanceof Diploma)
                    check = !((Diploma) list.get(i)).getFullName().contentEquals(listView.getItems().get(i));
                if (check) {
                    newRec = true;
                    break;
                }
            }
        }
        if (newRec) {
            listView.getItems().clear();
            for (DBEntity entity : list) {
                String newName = "";
                if (entity instanceof Subject)
                    newName = ((Subject) entity).getFullName();
                else if (entity instanceof Position)
                    newName = ((Position) entity).getFullName();
                else if (entity instanceof Diploma)
                    newName = ((Diploma) entity).getFullName();
                listView.getItems().add(newName);
            }
            if (listView.getItems().size() > index)
                listView.getSelectionModel().select(index);
        }
    }

    private String getCellValue(Cell cell) {
        if (cell == null)
            return "";
        switch (cell.getCellType()) {
            case Cell.CELL_TYPE_BLANK:
                return "";
            case Cell.CELL_TYPE_BOOLEAN:
                return Boolean.toString(cell.getBooleanCellValue());
            case Cell.CELL_TYPE_ERROR:
                return Byte.toString(cell.getErrorCellValue());
            case Cell.CELL_TYPE_FORMULA:
                return cell.getCellFormula();
            case Cell.CELL_TYPE_NUMERIC:
                return Long.toString((long) cell.getNumericCellValue());
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();
        }
        return "";
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    private void parseExcel(File file) throws Exception {
        Workbook wb;
        if (file.getName().substring(file.getName().length() - 3).equals("xls")) {
            wb = new HSSFWorkbook(new FileInputStream(file));
            ExcelExtractor extractor = new ExcelExtractor((HSSFWorkbook) wb);
            extractor.setFormulasNotResults(false);
            extractor.setIncludeSheetNames(true);
        } else {
            OPCPackage opcPackage = OPCPackage.open(file.getAbsolutePath());
            wb = new XSSFWorkbook(opcPackage);
            XSSFExcelExtractor extractor = new XSSFExcelExtractor((XSSFWorkbook) wb);
            extractor.setFormulasNotResults(false);
            extractor.setIncludeSheetNames(true);
        }
        Sheet sheet = wb.getSheetAt(0);
        Iterator<Row> it = sheet.iterator();
        Organization organization = new Organization();
        ArrayList<Sponsor> sponsorList = new ArrayList<>();
        ArrayList<Participant> participantList = new ArrayList<>();
        ArrayList<Mentor> mentorList = new ArrayList<>();
        ArrayList<Subject> subjectList = new ArrayList<>();
        ArrayList<Position> positionList = new ArrayList<>();
        while (it.hasNext()) {
            Row row = it.next();
            Iterator<Cell> cells = row.iterator();
            while (cells.hasNext()) {
                Cell cell = cells.next();
                switch (cell.getAddress().toString()) {
                    case "G7":
                        organization.setName(getCellValue(cell));
                        break;
                    case "F9":
                        organization.setAddress(getCellValue(cell));
                        break;
                    case "C11":
                        organization.setPhone(getCellValue(cell));
                        break;
                    case "J11":
                        organization.setEmail(getCellValue(cell));
                        break;
                }
            }
            if (row.getRowNum() > 15 && row.getRowNum() < 26) {
                if (getCellValue(row.getCell(2)).isEmpty() && getCellValue(row.getCell(5)).isEmpty())
                    continue;
                Sponsor sponsor = new Sponsor();
                sponsor.setFio(getCellValue(row.getCell(2)), getCellValue(row.getCell(5)), getCellValue(row.getCell(7)));
                String positionFullName = getCellValue(row.getCell(9));
                Position position = (Position) DBConnector.getInstance().find(DBEntity.EntityType.POSITION, "name", positionFullName);
                if (position == null) {
                    position = new Position(0, positionFullName, positionFullName);
                    positionList.add(position);
                }
                sponsor.setPosition(getCellValue(row.getCell(9)));
                sponsor.setPhone(getCellValue(row.getCell(12)));
                String persAgrt = getCellValue(row.getCell(14));
                if (persAgrt.contentEquals("согласен (на)"))
                    sponsor.setPersonalAgreement(true);
                else
                    sponsor.setPersonalAgreement(false);
                sponsorList.add(sponsor);
            }
            if (row.getRowNum() >= 34) {
                if (getCellValue(row.getCell(2)).isEmpty() && getCellValue(row.getCell(6)).isEmpty())
                    continue;
                Participant participant = new Participant();
                participant.setName(StringUtils.removeExtraSpaces(getCellValue(row.getCell(2))));
                participant.setGrade(getCellValue(row.getCell(6)));
                String subjectFullName = getCellValue(row.getCell(7));
                Subject subject = (Subject) DBConnector.getInstance().find(DBEntity.EntityType.SUBJECT, "name", subjectFullName);
                if (subject == null) {
                    subject = new Subject(0, subjectFullName, subjectFullName);
                    subjectList.add(subject);
                }
                participant.setSubject(subjectFullName);
                participant.setWorkName(getCellValue(row.getCell(8)));
                ArrayList<String> mentorFullNames = StringUtils.split(getCellValue(row.getCell(12)), ",");
                for (String mentorFullName : mentorFullNames) {
                    mentorFullName = StringUtils.removeExtraSpaces(mentorFullName.trim());
                    Mentor mentor = (Mentor) DBConnector.getInstance().find(DBEntity.EntityType.MENTOR, "name", mentorFullName);
                    if (mentor == null || mentor.getOrganization() != organization.getId()) {
                        mentor = new Mentor(0, organization.getId(), mentorFullName, selectedDiplomaForRequest, "", "");
                        mentorList.add(mentor);
                    }
                }
                participant.setMentor(getCellValue(row.getCell(12)));
                String persAgrt = getCellValue(row.getCell(15));
                if (persAgrt.contentEquals("согласен (на)"))
                    participant.setPersonalAgreement(true);
                else
                    participant.setPersonalAgreement(false);
                participant.setPlace(getCellValue(row.getCell(16)));
                participant.setScore(getCellValue(row.getCell(17)));
                participantList.add(participant);
            }
        }
        organization.setRequest(file.getAbsolutePath());
        DBConnector.getInstance().write(organization);
        for (Sponsor sponsor : sponsorList) {
            sponsor.setOrganization(organization.getId());
            sponsor.setThanksTemplate(selectedDiplomaForRequest);
            DBConnector.getInstance().write(sponsor);
        }
        for (Participant participant : participantList) {
            participant.setOrganization(organization.getId());
            participant.setDiploma(selectedDiplomaForRequest);
            DBConnector.getInstance().write(participant);
        }
        for (Mentor mentor : mentorList) {
            mentor.setOrganization(organization.getId());
            mentor.setThanksTemplate(selectedDiplomaForRequest);
            DBConnector.getInstance().write(mentor);
        }
        for (Subject subject : subjectList) {
            DBConnector.getInstance().write(subject);
        }
        for (Position position : positionList) {
            DBConnector.getInstance().write(position);
        }
    }

    public static MainController getInstance() {
        return instance;
    }

    public int getSelectedLVIndex(DBEntity.EntityType entityType) {
        switch (entityType) {
            case DIPLOMA:
                return lvDiplomas.getSelectionModel().getSelectedIndex();
            case MENTOR:
                return lvMentorOrganizations.getSelectionModel().getSelectedIndex();
            case ORGANIZATION:
                return tvOrganizations.getSelectionModel().getSelectedIndex();
            case PARTICIPANT:
                return lvParticipantOrganizations.getSelectionModel().getSelectedIndex();
            case POSITION:
                return lvPositions.getSelectionModel().getSelectedIndex();
            case SPONSOR:
                return lvSponsorOrganizations.getSelectionModel().getSelectedIndex();
            default:
                return lvSubjects.getSelectionModel().getSelectedIndex();
        }
    }

    private PrintSettingsController.PrintMode openPrintModeDialog() {
        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/main/org.helen.diplomacreator.view/PrintSettings.fxml"));
        Stage dialogStage = new Stage();
        Pane page = null;
        try {
            page = loader.load();
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
        Scene scene = new Scene(page);
        String css = this.getClass().getResource("/main/org.helen.diplomacreator.view/main.css").toExternalForm();
        PrintSettingsController controller = loader.getController();
        controller.setStage(dialogStage);
        scene.getStylesheets().add(css);
        dialogStage.setScene(scene);
        dialogStage.initModality(Modality.APPLICATION_MODAL);
        dialogStage.showAndWait();
        return controller.getPrintMode();
    }

}
