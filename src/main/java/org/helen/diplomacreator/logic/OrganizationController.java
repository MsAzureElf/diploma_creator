package org.helen.diplomacreator.logic;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.helen.diplomacreator.model.DBEntity;
import org.helen.diplomacreator.model.Organization;

/**
 * Created by Helen on 14.02.2016.
 */
public class OrganizationController implements FormController {

    @FXML
    public TextField tfName;
    @FXML
    public TextField tfPhone;
    @FXML
    public TextField tfEmail;
    @FXML
    public TextField tfAddress;

    @FXML
    private Button okBtn;
    @FXML
    private Button cancelBtn;
    @FXML
    private Button deleteBtn;

    private Organization data;
    private DialogResult dialogResult = DialogResult.CANCEL;
    private Stage stage;

    @FXML
    private void initialize() {
        okBtn.setOnAction(event -> {
            data.setName(tfName.getText());
            data.setEmail(tfEmail.getText());
            data.setPhone(tfPhone.getText());
            data.setAddress(tfAddress.getText());
            dialogResult = DialogResult.OK;
            stage.close();
        });
        cancelBtn.setOnAction(event -> {
            dialogResult = DialogResult.CANCEL;
            stage.close();
        });
        deleteBtn.setOnAction(event -> {
            dialogResult = DialogResult.DELETE;
            stage.close();
        });
    }

    @Override
    public void setData(DBEntity organizationData) {
        if (!(organizationData instanceof Organization))
            this.data = new Organization();
        else
            this.data = (Organization) organizationData;
        tfName.setText(data.getName());
        tfAddress.setText(data.getAddress());
        tfEmail.setText(data.getEmail());
        tfPhone.setText(data.getPhone());
    }

    @Override
    public DialogResult getDialogResult() {
        return dialogResult;
    }

    @Override
    public DBEntity getData() {
        return data;
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
