package org.helen.diplomacreator.logic;

import javafx.stage.Stage;
import org.helen.diplomacreator.model.DBEntity;

/**
 * Created by Helen on 21.02.2016.
 */
public interface FormController<T extends DBEntity> {

    void setData(T data);

    DialogResult getDialogResult();

    enum DialogResult {
        OK,
        CANCEL,
        DELETE;
    }

    T getData();

    void setStage(Stage stage);

}
