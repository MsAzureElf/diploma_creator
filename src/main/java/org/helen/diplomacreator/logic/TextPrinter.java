package org.helen.diplomacreator.logic;

import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextAttribute;
import java.awt.font.TextLayout;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.Hashtable;

/**
 * Created by Helen on 27.02.2016.
 */
public class TextPrinter {

    private LineBreakMeasurer lineMeasurer;
    private int paragraphStart;
    private int paragraphEnd;

    private Hashtable<TextAttribute, Object> map = new Hashtable<>();

    public TextPrinter(float fontSize) {
        map.put(TextAttribute.FAMILY, "Times New Roman");
        map.put(TextAttribute.SIZE, fontSize);
        map.put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
    }

    public int printText(Graphics2D g, String text, float width, float x, float y) {
        AttributedString string = new AttributedString(text, map);
        AttributedCharacterIterator paragraph = string.getIterator();
        paragraphStart = paragraph.getBeginIndex();
        paragraphEnd = paragraph.getEndIndex();
        FontRenderContext frc = g.getFontRenderContext();
        lineMeasurer = new LineBreakMeasurer(paragraph, frc);
        lineMeasurer.setPosition(paragraphStart);
        while (lineMeasurer.getPosition() < paragraphEnd) {
            TextLayout layout = lineMeasurer.nextLayout(width);
            y += layout.getAscent();
            layout.draw(g, x - (int) (layout.getBounds().getWidth() / 2), y);
            y += layout.getDescent() + layout.getLeading();
        }
        return (int) y;
    }

}
