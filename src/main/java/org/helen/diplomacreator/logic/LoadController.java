package org.helen.diplomacreator.logic;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.Stage;
import org.helen.diplomacreator.model.DBEntity;
import org.helen.diplomacreator.model.Diploma;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Helen on 13.03.2016.
 */
public class LoadController {

    private Stage stage;

    public ComboBox cbDiploma;
    public Button okBtn;

    private Map<String, String> map = new HashMap<>();

    @FXML
    private void initialize() {
        ArrayList<DBEntity> list = DBConnector.getInstance().readAll(DBEntity.EntityType.DIPLOMA);
        for (DBEntity entity : list) {
            cbDiploma.getItems().add(((Diploma) entity).getCompetitionName());
            map.put(((Diploma) entity).getCompetitionName(), ((Diploma) entity).getFullName());
        }
        okBtn.setOnAction(event -> {
            stage.close();
        });
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public String getSelectedDiploma() {
        if (cbDiploma.getSelectionModel().getSelectedIndex() == -1)
            return "";
        else return (String) cbDiploma.getSelectionModel().getSelectedItem();
    }

}
