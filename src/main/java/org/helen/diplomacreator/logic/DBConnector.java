package org.helen.diplomacreator.logic;

import org.helen.diplomacreator.model.DBEntity;
import org.helen.diplomacreator.model.EntityFactory;
import org.helen.diplomacreator.utils.Logger;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Helen on 06.02.2016.
 */
public class DBConnector {

    private static final String dbPath = "";
    private static final String CURRENT_DATABASE = "diploma_creator.db";
    private Statement statement;
    private Connection connection;

    private String[] tableNames = {"Diploma", "Mentor", "Organization", "Participant", "Position", "Sponsor", "Subject"};

    private String[] tableCreateStrings = {
            "CREATE TABLE [Diploma] (\n" +
                    "[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                    "[name] NVARCHAR(500)  NULL,\n" +
                    "[x] INTEGER NULL,\n" +
                    "[y] INTEGER NULL,\n" +
                    "[diploma_template] NVARCHAR(500)  NULL,\n" +
                    "[olympiad] BOOLEAN  NULL,\n" +
                    "[thanks_text_mentor] NVARCHAR(1000)  NULL,\n" +
                    "[thanks_text_sponsor] NVARCHAR(1000)  NULL,\n" +
                    "[competition_type] NVARCHAR(500)  NULL,\n" +
                    "[competition_name] NVARCHAR(500)  NULL\n" +
                    ")",
            "CREATE TABLE [Mentor] (\n" +
                    "[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                    "[name] NVARCHAR(400)  NULL,\n" +
                    "[organization] INTEGER NULL,\n" +
                    "[subject] NVARCHAR(100) NULL,\n" +
                    "[thanks_template] NVARCHAR(500) NULL,\n" +
                    "[position] NVARCHAR(100) NULL\n" +
                    ")",
            "CREATE TABLE [Organization] (\n" +
                    "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                    "[name] NVARCHAR(500)  NULL,\n" +
                    "[address] NVARCHAR(150)  NULL,\n" +
                    "[phone] NVARCHAR(20)  NULL,\n" +
                    "[email] NVARCHAR(200)  NULL,\n" +
                    "[request] NVARCHAR(1000)  NOT NULL\n" +
                    ")",
            "CREATE TABLE [Participant] (\n" +
                    "[id] INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,\n" +
                    "[name] NVARCHAR(400)  NULL,\n" +
                    "[grade] NVARCHAR(10)  NULL,\n" +
                    "[subject] NVARCHAR(100)  NULL,\n" +
                    "[work_name] NVARCHAR(500)  NULL,\n" +
                    "[score] NVARCHAR(10)  NULL,\n" +
                    "[place] NVARCHAR(100)  NULL,\n" +
                    "[mentor] NVARCHAR(100)  NULL,\n" +
                    "[diploma] NVARCHAR(500)  NULL,\n" +
                    "[organization] INTEGER  NULL,\n" +
                    "[agreement] BOOLEAN NOT NULL\n" +
                    ")",
            "CREATE TABLE [Position] (\n" +
                    "[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                    "[name] NVARCHAR(200)  NULL,\n" +
                    "[short_name] NVARCHAR(50)  NULL\n" +
                    ")",
            "CREATE TABLE [Sponsor] (\n" +
                    "[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                    "[first_name] NVARCHAR(100)  NULL,\n" +
                    "[last_name] NVARCHAR(100)  NULL,\n" +
                    "[father_name] NVARCHAR(100)  NULL,\n" +
                    "[position] NVARCHAR(250)  NULL,\n" +
                    "[phone] NVARCHAR(20)  NULL,\n" +
                    "[agreement] BOOLEAN  NOT NULL,\n" +
                    "[thanks_template] NVARCHAR(500) NULL,\n" +
                    "[organization] INTEGER  NULL\n" +
                    ")",
            "CREATE TABLE [Subject] (\n" +
                    "[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                    "[name] NVARCHAR(200)  NULL,\n" +
                    "[short_name] NVARCHAR(50)  NULL\n" +
                    ")"
    };

    private DBConnector() {
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:" + dbPath + CURRENT_DATABASE);
            statement = connection.createStatement();
            for (int i = 0; i < tableNames.length; ++i) {
                String checkTableExistanceQuery = "SELECT name FROM sqlite_master WHERE type='table' AND name='" + tableNames[i] + "'";
                if (!statement.executeQuery(checkTableExistanceQuery).next())
                    statement.execute(tableCreateStrings[i]);
            }
            statement.execute("PRAGMA case_sensitive_like=OFF;");
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (Exception e) {
            Logger.getInstance().log("Невозможно завершить текущий сеанс работы с базой данных" + dbPath + CURRENT_DATABASE);
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    public void write(DBEntity entity) {
        try {
            DBEntity entityWithId = find(entity);
            if (entityWithId != null) {
                entity.setId(entityWithId.getId());
                return;
            }
            statement.execute(entity.DBExportString());
            ArrayList<DBEntity> list = readAll(entity.getEntityType());
            entityWithId = find(entity);
            if (entityWithId != null)
                entity.setId(entityWithId.getId());
        } catch (Exception e) {
            Logger.getInstance().log("Database write error");
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    public void update(DBEntity entity) {
        try {
            statement.execute(entity.DBModifyString());
        } catch (Exception e) {
            Logger.getInstance().log("Database update error");
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    public void delete(DBEntity entity) {
        try {
            statement.execute("DELETE from " + entity.getEntityType().getValue() + " where id = " + entity.getId());
        } catch (Exception e) {
            Logger.getInstance().log("Database delete error");
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    public ArrayList<DBEntity> readAll(DBEntity.EntityType entityType) {
        ArrayList<DBEntity> result = new ArrayList<>();
        try {
            ResultSet set = statement.executeQuery("SELECT * from " + entityType.name()/*+" order by id asc"*/);
            while (set.next()) {
                result.add(EntityFactory.getEntity(set, entityType));
            }
        } catch (Exception e) {
            Logger.getInstance().log("Unable to read database");
            e.printStackTrace(Logger.getInstance().getStream());
        }
        return result;
    }

    public ArrayList<DBEntity> read(DBEntity.EntityType entityType, String field, Object constraint, boolean like) {
        ArrayList<DBEntity> result = new ArrayList<>();
        try {
            String constraintStr;
            if (constraint instanceof Number)
                constraintStr = constraint.toString();
            else
                constraintStr = "'" + (like?"%":"") + constraint.toString() + (like?"%":"") + "'";
            ResultSet set = statement.executeQuery("SELECT * from " + entityType.name() + " where " + field + (like ? " LIKE " : " = ") + constraintStr);
            while (set.next()) {
                result.add(EntityFactory.getEntity(set, entityType));
            }
        } catch (Exception e) {
            Logger.getInstance().log("Unable to read database");
            e.printStackTrace(Logger.getInstance().getStream());
        }
        return result;
    }

    public DBEntity find(DBEntity.EntityType entityType, String field, String value) {
        try {
            ResultSet set = statement.executeQuery("SELECT * from " + entityType.name() + " where " + field + " = '" + value + "'");
            if (set == null)
                return null;
            else {
                if (!set.next())
                    return null;
                return EntityFactory.getEntity(set, entityType);
            }
        } catch (Exception e) {
            Logger.getInstance().log("Unable to read database");
            e.printStackTrace(Logger.getInstance().getStream());
            return null;
        }
    }

    public DBEntity find(DBEntity entity) {
        try {
            ResultSet set = statement.executeQuery(entity.DBFindString());
            if (set == null)
                return null;
            else {
                if (!set.next())
                    return null;
                return EntityFactory.getEntity(set, entity.getEntityType());
            }
        } catch (Exception e) {
            Logger.getInstance().log("Unable to read database");
            e.printStackTrace(Logger.getInstance().getStream());
            return null;
        }
    }

    private static class SingletonHolder {

        public static final DBConnector instance = new DBConnector();

    }

    public static DBConnector getInstance() {

        return SingletonHolder.instance;

    }

}