package org.helen.diplomacreator.logic;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.ToggleGroup;
import javafx.stage.Stage;

/**
 * Created by Helen on 27.03.2016.
 */
public class PrintSettingsController {

    private Stage stage;

    public RadioButton rbAllOrganizations;
    public RadioButton rbSelectedSponsor;
    public RadioButton rbSelectedMentor;
    public RadioButton rbSelectedOrganization;
    public RadioButton rbSelectedParticipant;
    public Button okBtn;

    private PrintMode printMode = PrintMode.NONE;

    @FXML
    private void initialize() {
        okBtn.setOnAction(event -> {
            if (rbAllOrganizations.isSelected())
                printMode = PrintMode.PRINT_ALL;
            if (rbSelectedOrganization.isSelected())
                printMode = PrintMode.PRINT_ORGANIZATION;
            if (rbSelectedParticipant.isSelected())
                printMode = PrintMode.PRINT_PARTICIPANT;
            if (rbSelectedMentor.isSelected())
                printMode = PrintMode.PRINT_MENTOR;
            if (rbSelectedSponsor.isSelected())
                printMode = PrintMode.PRINT_SPONSOR;
            stage.close();
        });
        ToggleGroup group = new ToggleGroup();
        rbAllOrganizations.setToggleGroup(group);
        rbSelectedOrganization.setToggleGroup(group);
        rbSelectedParticipant.setToggleGroup(group);
        rbSelectedMentor.setToggleGroup(group);
        rbSelectedSponsor.setToggleGroup(group);
        rbSelectedOrganization.setSelected(true);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public PrintMode getPrintMode() {
        return printMode;
    }

    public enum PrintMode {
        PRINT_ALL,
        PRINT_ORGANIZATION,
        PRINT_PARTICIPANT,
        PRINT_MENTOR,
        PRINT_SPONSOR,
        NONE
    }

}
