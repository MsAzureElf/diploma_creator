package org.helen.diplomacreator.logic;

import org.helen.diplomacreator.model.*;
import org.helen.diplomacreator.utils.Logger;
import org.helen.diplomacreator.utils.StringUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.geom.AffineTransform;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by Helen on 26.02.2016.
 */
public class DiplomaPrinter {

    private String HEADER = "Всероссийский центр гражданских и молодёжных инициатив \"Идея\"\n";
    private static final String DIPLOMA_CAPTION = "Диплом";
    private static final String THANKS_CAPTION = "Благодарственное письмо";
    private static final String CERTIFICATE_CAPTION_TOP = "Сертификат";
    private static final String CERTIFICATE_CAPTION_BOTTOM = "участника";
    private static final String NUMBER = "№ И-";
    private static final String AWARDED = "награждается";
    private static final String WINNER = "победитель";
    private static final String AWARDEE = "призер";

    private Diploma diploma;
    private TextPrinter textPrinterSmall;
    private TextPrinter textPrinterLarge;
    private TextPrinter textPrinterLargeH3;
    private TextPrinter textPrinterExtraLarge;
    public static final String []forbiddenCharacters = {"*", "|", "\\", ":", "\"", "<", ">", "?", "/"};

    enum DiplomaDegree {
        FIRST("I степени"),
        SECOND("II степени"),
        THIRD("III степени");

        private final String value;

        DiplomaDegree(String value) {
            this.value = value;
        }

        static public DiplomaDegree getEntityType(String entityType) {
            for (DiplomaDegree type : DiplomaDegree.values()) {
                if (type.toString().equalsIgnoreCase(entityType)) {
                    return type;
                }
            }
            return THIRD;
        }

        public String getValue() {
            return value;
        }

    }

    private BufferedImage template = null;
    private int xInit = 0, yInit = 0;

    public DiplomaPrinter(Diploma diploma) {
        this.diploma = diploma;
        textPrinterSmall = new TextPrinter(34);
        textPrinterLarge = new TextPrinter(38);
        textPrinterLargeH3 = new TextPrinter(42);
        textPrinterExtraLarge = new TextPrinter(60);
        try {
            template = ImageIO.read(new File(diploma.getDiplomaTemplate()));
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
        xInit = diploma.getX() * template.getWidth() / 100;
        yInit = diploma.getY() * template.getHeight() / 100;
    }

    public void createDiploma(Participant participant) {
        BufferedImage image = new BufferedImage(template.getWidth(), template.getHeight(), template.getType());
        Graphics graphics = image.getGraphics();
        graphics.drawImage(template, 0, 0, null);
        graphics.setColor(Color.black);
        Font h1 = new Font("Times New Roman", Font.BOLD, 72);
        Font h2 = new Font("Times New Roman", Font.BOLD, 60);
        Font h3 = new Font("Times New Roman", Font.BOLD, 42);
        Font h4 = new Font("Times New Roman", Font.BOLD, 38);
        Font h5 = new Font("Times New Roman", Font.PLAIN, 38);
        Font h6 = new Font("Times New Roman", Font.BOLD, 34);
        Font h7 = new Font("Times New Roman", Font.BOLD, 12);
        FontRenderContext frc = new FontRenderContext(new AffineTransform(), true, true);
        int y = drawString(HEADER, graphics, frc, h6, yInit, 0);
        if (isPrizeWinner(participant))
            y = drawString(DIPLOMA_CAPTION, graphics, frc, h1, y, 3);
        else
            y = drawString(CERTIFICATE_CAPTION_TOP, graphics, frc, h1, y, 3);
        y = drawString(NUMBER + participant.getId(), graphics, frc, h4, y, 3);
        Organization organization = (Organization) DBConnector.getInstance().find(DBEntity.EntityType.ORGANIZATION, "id", Integer.toString(participant.getOrganization()));
        if (isPrizeWinner(participant)) {
            if (participant.getPlace().contentEquals("1"))
                y = drawString(DiplomaDegree.FIRST.getValue(), graphics, frc, h3, y, 3);
            else if (participant.getPlace().contentEquals("2"))
                y = drawString(DiplomaDegree.SECOND.getValue(), graphics, frc, h3, y, 3);
            else if (participant.getPlace().contentEquals("3"))
                y = drawString(DiplomaDegree.THIRD.getValue(), graphics, frc, h3, y, 3);
            y = drawString(AWARDED, graphics, frc, h4, y, 3);
            y = drawString("", graphics, frc, h7, y, 1);
            y = textPrinterExtraLarge.printText((Graphics2D) graphics, participant.getName(),
                    3 * template.getWidth() / 5, xInit, y);
            y = drawString("", graphics, frc, h7, y, 1);
            if (organization != null)
                y = textPrinterLarge.printText((Graphics2D) graphics, organization.getName(), 3 * template.getWidth() / 5, xInit, y);
            else
                y = drawString("", graphics, frc, h4, y, 3);
            if (participant.getPlace().contentEquals("1"))
                y = drawString(WINNER, graphics, frc, h5, y, 3);
            else
                y = drawString(AWARDEE, graphics, frc, h5, y, 3);
            y = drawString("", graphics, frc, h7, y, 1);
            y = textPrinterLargeH3.printText((Graphics2D) graphics, diploma.getCompetitionType(), 3 * template.getWidth() / 5, xInit, y);
            //y = drawString(diploma.getCompetitionType(), graphics, frc, h3, y, 3);
            y = drawString("", graphics, frc, h7, y, 1);
            y = textPrinterExtraLarge.printText((Graphics2D) graphics, "\"" + diploma.getCompetitionName() + "\"",
                    3 * template.getWidth() / 5, xInit, y);
        } else {
            y = drawString(CERTIFICATE_CAPTION_BOTTOM, graphics, frc, h3, y, 3);
            //y = textPrinterLarge.printText((Graphics2D) graphics, diploma.getCompetitionType(), 3 * template.getWidth() / 5, xInit, y);
            y = drawString("", graphics, frc, h7, y, 1);
            y = textPrinterLargeH3.printText((Graphics2D) graphics, diploma.getCompetitionType(), 3 * template.getWidth() / 5, xInit, y);
            y = drawString("", graphics, frc, h7, y, 1);
            y = textPrinterExtraLarge.printText((Graphics2D) graphics, "\"" + diploma.getCompetitionName() + "\"",
                    3 * template.getWidth() / 5, xInit, y);
            y = drawString(AWARDED, graphics, frc, h4, y, 3);
            y = drawString("", graphics, frc, h7, y, 1);
            y = textPrinterExtraLarge.printText((Graphics2D) graphics, participant.getName(),
                    3 * template.getWidth() / 5, xInit, y);
            y = drawString("", graphics, frc, h7, y, 1);
            if (organization != null)
                y = textPrinterLarge.printText((Graphics2D) graphics, organization.getName(), 3 * template.getWidth() / 5, xInit, y);
            else
                y = drawString("", graphics, frc, h4, y, 3);
        }
        String nomination;
        if (diploma.isOlympiad())
            nomination = "Предмет: ";
        else
            nomination = "Номинация: ";
        if (!participant.getSubject().isEmpty())
            y = drawString(nomination + participant.getSubject(), graphics, frc, h6, y, 3);
        if (!participant.getWorkName().isEmpty()) {
            y = drawString("", graphics, frc, h7, y, 1);
            String workName;
            if (diploma.isOlympiad())
                workName = "Тема: ";
            else
                workName = "Название работы: ";
            y = textPrinterSmall.printText((Graphics2D) graphics, workName + participant.getWorkName(), 3 * template.getWidth() / 5, xInit, y);
        }
        if (!isPrizeWinner(participant) && !participant.getScore().isEmpty())
            y = drawString("Балл - " + participant.getScore(), graphics, frc, h6, y, 2);
        ArrayList name = StringUtils.split(participant.getName(), " ");
        if (((name != null && name.size() != 3) || participant.getName().contains(",")) && !participant.getMentor().isEmpty())
            drawString("Руководитель: " + participant.getMentor(), graphics, frc, h6, y, 2);
        File diplomaFile = new File(System.getProperty("user.dir") + "\\" + diploma.getCompetitionName().trim());
        if (!diplomaFile.exists() || !diplomaFile.isDirectory())
            diplomaFile.mkdir();
        File organizationFile = new File(diplomaFile.getAbsolutePath() + "\\" + StringUtils.replaceForbidden(organization.getName(), forbiddenCharacters).trim());
        if (!organizationFile.exists() || !organizationFile.isDirectory())
            organizationFile.mkdir();
        String participantName = StringUtils.replaceForbidden(participant.getName(), forbiddenCharacters).trim();
        File participantFile = new File(organizationFile.getAbsolutePath() + "\\" + participantName + ".png");
        int counter = 1;
        while (participantFile.exists()){
            participantFile = new File(organizationFile.getAbsolutePath() + "\\" + participantName + " (" + counter + ").png");
            counter++;
        }
        try {
            ImageIO.write(image, "png", participantFile);
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    private void createThanksLetter(String name, String position, int orgn, boolean mentor) {
        BufferedImage image = new BufferedImage(template.getWidth(), template.getHeight(), template.getType());
        Graphics graphics = image.getGraphics();
        graphics.drawImage(template, 0, 0, null);
        graphics.setColor(Color.black);
        Font h1 = new Font("Times New Roman", Font.BOLD, 72);
        Font h2 = new Font("Times New Roman", Font.BOLD, 60);
        Font h4 = new Font("Times New Roman", Font.BOLD, 38);
        Font h6 = new Font("Times New Roman", Font.BOLD, 34);
        Font h7 = new Font("Times New Roman", Font.BOLD, 12);
        FontRenderContext frc = new FontRenderContext(new AffineTransform(), true, true);
        int y = drawString(HEADER, graphics, frc, h6, yInit, 0);
        y = drawString(THANKS_CAPTION, graphics, frc, h1, y, 6);
        y = drawString(name, graphics, frc, h2, y, 5);
        if (!position.isEmpty())
            y = drawString(position, graphics, frc, h4, y, 4);
        Organization organization = (Organization) DBConnector.getInstance().find(DBEntity.EntityType.ORGANIZATION, "id", Integer.toString(orgn));
        y = drawString("", graphics, frc, h7, y, 3);
        if (organization != null)
            y = textPrinterLarge.printText((Graphics2D) graphics, organization.getName(), 3 * template.getWidth() / 5, xInit, y);
        else
            y = drawString("", graphics, frc, h4, y, 3);
        y = drawString("", graphics, frc, h7, y, 1);
        y = drawLines(mentor ? diploma.getThanksTextMentor() : diploma.getThanksTextSponsor(), graphics, frc, h4, y, 1);
        y = drawString("", graphics, frc, h7, y, 1);
        y = textPrinterExtraLarge.printText((Graphics2D) graphics, "\"" + diploma.getCompetitionName() + "\"",
                3 * template.getWidth() / 5, xInit, y);
        File diplomaFile = new File(System.getProperty("user.dir") + "\\" + diploma.getCompetitionName().trim());
        if (!diplomaFile.exists() || !diplomaFile.isDirectory())
            diplomaFile.mkdir();
        String organizationName = organization.getName();
        for (String forbidden : forbiddenCharacters)
            organizationName = organizationName.replace(forbidden, "");
        File organizationFile = new File(diplomaFile.getAbsolutePath() + "\\" + organizationName.trim());
        if (!organizationFile.exists() || !organizationFile.isDirectory())
            organizationFile.mkdir();
        name = StringUtils.replaceForbidden(name, forbiddenCharacters).trim();
        File participantFile = new File(organizationFile.getAbsolutePath() + "\\" + name + ".png");
        int counter = 1;
        while (participantFile.exists()){
            participantFile = new File(organizationFile.getAbsolutePath() + "\\" + name + " (" + counter + ").png");
            counter++;
        }
        try {
            ImageIO.write(image, "png", participantFile);
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    public void createThanksLetter(Mentor mentor) {
        createThanksLetter(mentor.getName(), mentor.getPosition(), mentor.getOrganization(), true);
    }

    public void createThanksLetter(Sponsor sponsor) {
        createThanksLetter(sponsor.getFirstName() + " " + sponsor.getLastName() + " " + sponsor.getFatherName(),
                sponsor.getPosition(), sponsor.getOrganization(), false);
    }

    private int drawString(String text, Graphics graphics, FontRenderContext frc, Font font, int y, int percentIncrease) {
        Rectangle2D textSize = font.getStringBounds(text, frc);
        graphics.setFont(font);
        graphics.drawString(text, (int) (xInit - textSize.getWidth() / 2), (int) (y + textSize.getHeight() / 2 + template.getHeight() * percentIncrease / 100));
        return (int) (y + textSize.getHeight() / 2 + template.getHeight() * percentIncrease / 100);
    }

    public static boolean isPrizeWinner(Participant participant) {
        if (participant.getPlace().contentEquals("1") || participant.getPlace().contentEquals("2") || participant.getPlace().contentEquals("3"))
            return true;
        return false;
    }

    private int drawLines(String text, Graphics graphics, FontRenderContext frc, Font font, int y, int percentIncrease) {
        int index = text.indexOf('\n'), prevPt = 0, indCheck = index;
        while (indCheck != -1) {
            y = drawString(text.substring(prevPt, index), graphics, frc, font, y, 2);
            prevPt = index + 1;
            indCheck = text.substring(prevPt).indexOf('\n');
            index = indCheck + prevPt;
        }
        y = drawString(text.substring(prevPt), graphics, frc, font, y, 2);
        return y + template.getHeight() * percentIncrease / 100;
    }

}