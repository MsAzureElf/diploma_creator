package org.helen.diplomacreator.logic;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.helen.diplomacreator.model.DBEntity;
import org.helen.diplomacreator.model.Diploma;

import java.util.ArrayList;

/**
 * Created by Helen on 13.03.2016.
 */
public class DiplomaSelectorController {

    public Label captionLbl;
    private Stage stage;

    public ComboBox cbDiploma;
    public Button okBtn;

    private FormController.DialogResult dialogResult = FormController.DialogResult.CANCEL;

    @FXML
    private void initialize() {
        ArrayList<DBEntity> list = DBConnector.getInstance().readAll(DBEntity.EntityType.DIPLOMA);
        for (DBEntity entity : list) {
            cbDiploma.getItems().add(((Diploma) entity).getCompetitionName());
        }
        if (list.size() > 0)
            cbDiploma.setValue(((Diploma) list.get(0)).getCompetitionName());
        okBtn.setOnAction(event -> {
            dialogResult = FormController.DialogResult.OK;
            stage.close();
        });
    }

    public void setCaption(String caption){
        captionLbl.setText(caption);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public String getSelectedDiploma() {
        if (cbDiploma.getSelectionModel().getSelectedIndex() == -1)
            return "";
        else return (String) cbDiploma.getSelectionModel().getSelectedItem();
    }

    public FormController.DialogResult getDialogResult() {
        return dialogResult;
    }

}
