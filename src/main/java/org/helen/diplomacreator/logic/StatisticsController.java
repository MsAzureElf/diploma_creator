package org.helen.diplomacreator.logic;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.helen.diplomacreator.model.DBEntity;
import org.helen.diplomacreator.model.Diploma;
import org.helen.diplomacreator.model.Participant;

import java.util.ArrayList;

/**
 * Created by Helen on 13.03.2016.
 */
public class StatisticsController {

    private Stage stage;

    public ComboBox cbDiploma;
    public TextField tfOverall;
    public TextField tfFirstPlace;
    public TextField tfSecondPlace;
    public TextField tfThirdPlace;
    public TextField tfCertificate;
    public TextField tfMentor;
    public TextField tfSponsor;
    public Button okBtn;

    @FXML
    private void initialize() {
        ArrayList<DBEntity> list = DBConnector.getInstance().readAll(DBEntity.EntityType.DIPLOMA);
        for (DBEntity entity : list) {
            cbDiploma.getItems().add(((Diploma) entity).getCompetitionName());
        }
        okBtn.setOnAction(event -> {
            stage.close();
        });
        if (list.size() > 0)
            cbDiploma.setValue(((Diploma)list.get(0)).getCompetitionName());
        cbDiploma.setOnAction(event -> {
            if (cbDiploma.getSelectionModel().getSelectedIndex() == -1)
                return;
            String diplomaFullName = (String) cbDiploma.getSelectionModel().getSelectedItem();
            ArrayList<DBEntity> participantList = DBConnector.getInstance().read(DBEntity.EntityType.PARTICIPANT, "diploma", diplomaFullName, false);
            ArrayList<DBEntity> mentorList = DBConnector.getInstance().read(DBEntity.EntityType.MENTOR, "thanks_template", diplomaFullName, false);
            ArrayList<DBEntity> sponsorList = DBConnector.getInstance().read(DBEntity.EntityType.SPONSOR, "thanks_template", diplomaFullName, false);
            tfOverall.setText(Integer.toString(participantList.size()));
            int[] count = {0, 0, 0, 0};
            for (DBEntity entity : participantList) {
                Participant participant = (Participant) entity;
                if (participant.getPlace().contentEquals("1"))
                    count[0]++;
                else if (participant.getPlace().contentEquals("2"))
                    count[1]++;
                else if (participant.getPlace().contentEquals("3"))
                    count[2]++;
                else
                    count[3]++;
            }
            tfFirstPlace.setText(Integer.toString(count[0]));
            tfSecondPlace.setText(Integer.toString(count[1]));
            tfThirdPlace.setText(Integer.toString(count[2]));
            tfCertificate.setText(Integer.toString(count[3]));
            tfMentor.setText(Integer.toString(mentorList.size()));
            tfSponsor.setText(Integer.toString(sponsorList.size()));
        });
        cbDiploma.fireEvent(new ActionEvent());
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

}
