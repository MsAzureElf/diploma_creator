package org.helen.diplomacreator.logic;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.helen.diplomacreator.model.*;
import org.helen.diplomacreator.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.Collectors;

/**
 * Created by Helen on 14.02.2016.
 */
public class ParticipantController implements FormController {

    @FXML
    public TextField tfGrade;
    @FXML
    public TextField tfWorkName;
    @FXML
    private TextField tfFirstName;
    @FXML
    private TextField tfLastName;
    @FXML
    private TextField tfFatherName;
    @FXML
    private TextField tfNumber;
    @FXML
    private ComboBox<String> cbSubject;
    @FXML
    private ComboBox<String> cbMentor;
    @FXML
    private TextField tfScore;
    @FXML
    private TextField tfPlace;
    @FXML
    public ComboBox<String> cbDiploma;

    @FXML
    private Button okBtn;
    @FXML
    private Button cancelBtn;
    @FXML
    private Button deleteBtn;

    private Participant data;
    private DialogResult dialogResult = DialogResult.CANCEL;
    private Stage stage;

    @FXML
    private void initialize() {
        okBtn.setOnAction(event -> {
            String name = tfFirstName.getText() + " " + tfLastName.getText();
            if (!tfFatherName.getText().isEmpty())
                name += " " + tfFatherName.getText();
            data.setName(name);
            data.setMentor(cbMentor.getValue());
            data.setSubject(cbSubject.getValue());
            data.setScore(tfScore.getText());
            data.setGrade(tfGrade.getText());
            data.setWorkName(tfWorkName.getText());
            data.setPlace(tfPlace.getText());
            data.setDiploma(cbDiploma.getValue());
            dialogResult = DialogResult.OK;
            stage.close();
        });
        cancelBtn.setOnAction(event -> {
            dialogResult = DialogResult.CANCEL;
            stage.close();
        });
        deleteBtn.setOnAction(event -> {
            dialogResult = DialogResult.DELETE;
            stage.close();
        });
        ArrayList<DBEntity> dipList = DBConnector.getInstance().readAll(DBEntity.EntityType.DIPLOMA);
        for (DBEntity entity : dipList) {
            cbDiploma.getItems().add(((Diploma) entity).getCompetitionName());
        }
        ObservableList<String> subjectList = FXCollections.observableArrayList();
        ArrayList<DBEntity> subjects = DBConnector.getInstance().readAll(DBEntity.EntityType.SUBJECT);
        subjectList.addAll(subjects.stream().map(entity -> ((Subject) entity).getFullName()).collect(Collectors.toList()));
        cbSubject.setItems(subjectList);
        ObservableList<String> mentorList = FXCollections.observableArrayList();
        ArrayList<DBEntity> mentors = DBConnector.getInstance().readAll(DBEntity.EntityType.MENTOR);
        ObservableSet<String> list = FXCollections.observableSet(new HashSet<String>());
        for (DBEntity mentor : mentors) {
            list.add(((Mentor) mentor).getName());
        }
        mentorList.addAll(list.stream().collect(Collectors.toList()));
        cbMentor.setItems(mentorList);
    }

    @Override
    public void setData(DBEntity participantData) {
        if (!(participantData instanceof Participant))
            this.data = new Participant();
        else
            this.data = (Participant) participantData;
        ArrayList<String> name =  StringUtils.split(data.getName(), " ");
        if (name != null && name.size() > 0)
            tfFirstName.setText(name.get(0));
        else
            tfFirstName.setText("");
        if (name != null && name.size() > 1)
            tfLastName.setText(name.get(1));
        else
            tfLastName.setText("");
        if (name != null && name.size() > 2)
            tfFatherName.setText(name.get(2));
        else
            tfFatherName.setText("");
        cbDiploma.setValue(data.getDiploma());
        tfNumber.setText(Integer.toString(data.getId()));
        tfNumber.setEditable(false);
        tfPlace.setText(data.getPlace());
        tfScore.setText(data.getScore());
        tfGrade.setText(data.getGrade());
        tfWorkName.setText(data.getWorkName());
        cbMentor.setValue(data.getMentor());
        cbSubject.setValue(data.getSubject());
    }

    @Override
    public DialogResult getDialogResult() {
        return dialogResult;
    }

    @Override
    public DBEntity getData() {
        return data;
    }

    @Override
    public void setStage(Stage stage) {
        this.stage = stage;
    }
}
