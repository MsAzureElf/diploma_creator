package org.helen.diplomacreator.logic;

import org.helen.diplomacreator.model.DBEntity;
import org.helen.diplomacreator.model.Organization;
import org.helen.diplomacreator.model.Participant;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.STMerge;
import org.helen.diplomacreator.utils.Logger;
import org.helen.diplomacreator.utils.StringUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Helen on 03.04.2016.
 */
public class ReportPrinter {

    private XWPFDocument document;
    private String diploma;
    private String[] header = {"№", "Название образовательного учреждения", "Список участников", "Номинация", "Итог"};

    enum PlaceString {

        FIRST("Диплом I степени"),
        SECOND("Диплом II степени"),
        THIRD("Диплом III степени"),
        NO("Сертификат");

        private String value;

        PlaceString(String value) {
            this.value = value;
        }

        public String toString() {
            return value;
        }

    }

    public void createReport(String diploma) {
        try {
            document = new XWPFDocument();
            this.diploma = diploma;
            FileOutputStream out = null;
            out = new FileOutputStream(new File(diploma + ".docx"));
            XWPFParagraph paragraph = document.createParagraph();
            paragraph.setAlignment(ParagraphAlignment.CENTER);
            XWPFRun run = paragraph.createRun();
            run.setText("Общий протокол конкурса \"" + diploma + "\"");
            createTable();
            document.write(out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace(Logger.getInstance().getStream());
        }
    }

    private void createTable() {
        XWPFTable table = document.createTable();
        XWPFTableRow tableHeader = table.getRow(0);
        tableHeader.getCell(0).setText(header[0]);
        for (int i = 1; i < header.length; ++i) {
            tableHeader.createCell().setText(header[i]);
        }
        ArrayList<DBEntity> list = DBConnector.getInstance().read(DBEntity.EntityType.PARTICIPANT, "diploma", diploma, false);
        Map<Integer, ArrayList<Participant>> map = new HashMap<>();
        for (DBEntity entity : list) {
            Participant participant = (Participant) entity;
            if (map.containsKey(participant.getOrganization()))
                map.get(participant.getOrganization()).add(participant);
            else {
                ArrayList<Participant> arrayList = new ArrayList<>();
                arrayList.add(participant);
                map.put(participant.getOrganization(), arrayList);
            }
        }
        int rowCount = 1;
        for (Map.Entry<Integer, ArrayList<Participant>> entry : map.entrySet()) {
            ArrayList<Participant> lst = entry.getValue();
            int N = lst.size();
            for (int i = 0; i < N; ++i) {
                table.createRow();
                XWPFTableRow row = table.getRow(rowCount + i);
                row.getCell(0).setText(Integer.toString(rowCount + i));
                ArrayList<String> names = StringUtils.split(lst.get(i).getName(), ",");
                String name = "";
                if (names.size() > 0)
                    name = shortenName(names.get(0));
                for (int j = 1; j < names.size(); ++j)
                    name += ", " + shortenName(names.get(j));
                row.getCell(2).setText(name);
                row.getCell(3).setText(lst.get(i).getSubject());
                String place = "";
                if (!lst.get(i).getScore().isEmpty())
                    place = lst.get(i).getScore() + "\\";
                if (lst.get(i).getPlace().contentEquals("1"))
                    place += PlaceString.FIRST.toString();
                else if (lst.get(i).getPlace().contentEquals("2"))
                    place += PlaceString.SECOND.toString();
                else if (lst.get(i).getPlace().contentEquals("3"))
                    place += PlaceString.THIRD.toString();
                else
                    place += PlaceString.NO.toString();
                row.getCell(4).setText(place);
            }
            mergeCellsVertically(table, 1, rowCount, rowCount + N);
            XWPFTableRow row = table.getRow(rowCount);
            DBEntity entity = DBConnector.getInstance().find(DBEntity.EntityType.ORGANIZATION, "id", entry.getKey().toString());
            row.getCell(1).setText(((Organization) entity).getName());
            rowCount += N;
        }
    }

    private static void mergeCellsVertically(XWPFTable table, int col, int from, int to) {
        for (int i = from; i < to; i++) {
            XWPFTableCell cell = table.getRow(i).getCell(col);
            if (i == from)
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.RESTART);
            else
                cell.getCTTc().addNewTcPr().addNewVMerge().setVal(STMerge.CONTINUE);
        }
    }

    private String shortenName(String name) {
        ArrayList<String> splittedName = StringUtils.split(name, " ");
        switch (splittedName.size()) {
            case 0:
                return "";
            case 1:
                return splittedName.get(0);
            case 2:
                return splittedName.get(0) + " " + splittedName.get(1).charAt(0) + ".";
            default:
                return splittedName.get(0) + " " + splittedName.get(1).charAt(0) + "." + splittedName.get(2).charAt(0) + ".";
        }
    }

}
